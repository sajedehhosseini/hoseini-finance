import axios from './Http'

export const adminLogin = (body) => {
    const url=`admin/login/`;
    return axios.post(url,body);
};
//courses api calls    todo: get data with id product and pagination
export const coursesList = () => {
    const url=`admin/product/`;
    return axios.get(url);
};
export const addCourse = (body) => {
    const url=`admin/product/`;
    return axios.post(url,body);
};
export const UpdateCourse = (id,body) => {
    const url=`admin/product/${id}`;
    return axios.patch(url,body);
};
export const deleteCourse = (id) => {
    const url=`admin/product/${id}`;
    return axios.delete(url);
};

//categories api calls    todo: get data with id product and pagination
export const categories = () => {
    const url=`admin/category/`;
    return axios.get(url);
};
export const addCategory = (body) => {
    const url=`admin/category/`;
    return axios.post(url,body);
};
export const UpdateCategory = (id,body) => {
    const url=`admin/category/${id}`;
    return axios.patch(url,body);
};
export const deleteCategory = (id) => {
    const url=`admin/category/${id}`;
    return axios.delete(url);
};

//discounts api calls
export const discounts = () => {
    const url=`admin/discount-product/`;
    return axios.get(url);
};
export const addDiscount = (body) => {
    const url=`admin/discount-product/`;
    return axios.post(url,body);
};


//factors api calls     todo: get data with id product and pagination
export const factors = () => {
    const url=`admin/factor/`;
    return axios.get(url);
};
export const addFactor = (body) => {
    const url=`admin/factor/`;
    return axios.post(url,body);
};
export const UpdateFactor = (id,body) => {
    const url=`admin/factor/${id}`;
    return axios.patch(url,body);
};
export const deleteFactor = (id) => {
    const url=`admin/factor/${id}`;
    return axios.delete(url);
};


//users api calls         todo: get data with id product and pagination
export const users = () => {
    const url=`admin/user/`;
    return axios.get(url);
};
export const addUser = (body) => {
    const url=`admin/user/`;
    return axios.post(url,body);
};
export const UpdateUser = (id,body) => {
    const url=`admin/user/${id}`;
    return axios.patch(url,body);
};
export const deleteUser = (id) => {
    const url=`admin/user/${id}/`;
    return axios.delete(url);
};


//admins api calls     ==> link nadashtm
export const admins = () => {
    const url=`admin/user/`;
    return axios.get(url);
};

//tickets api calls  ===> sendAnswerTicket baraye chie?          todo: get data with id product and pagination
export const tickets = () => {
    const url=`admin/ticket/`;
    return axios.get(url);
};
export const sendAnswerTicket = (body) => {
    const url=`admin/ticket/`;
    return axios.post(url,body);
};