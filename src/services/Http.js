import axios from "axios";
import { store } from "../store/store";
const baseUrl = "http://77.237.77.250:8080/";
axios.interceptors.request.use(
  (config) => {
    let { url } = config;
    // console.log("config", config);
    // console.log("config", url.split("/")[0]);
    const { access } = store.getState().user;
    config.baseURL = baseUrl;
    if (
      ["user", "admin"].indexOf(url.split("/")[0]) > -1 &&
      ["login", "register","create_token"].indexOf(url.split("/")[1]) === -1
    ) {
      config.headers["Authorization"] = "Bearer " + access;
    }
    return config;
  },
  (error) => {
    return Promise.reject(error);
  }
);
axios.interceptors.response.use(
  (response) => {
    return response;
  },
  (error) => {
    return Promise.reject(error);
  }
);

export default {
  axios,
  get: axios.get,
  post: axios.post,
  put: axios.put,
  delete: axios.delete,
  patch: axios.patch,
};

// const myInterceptor = axios.interceptors.request.use(function () {/*...*/});
// axios.interceptors.request.eject(myInterceptor);
