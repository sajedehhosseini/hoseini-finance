import { useRouter } from "next/router";
import { store } from "../store/store";
const withPrivateRoute = (Component) => {
  const Auth = (props) => {
    const router = useRouter();
    const user = store.getState().user;
    // console.log("withPrivateRoute", store.getState().user.access);

    if (user && user instanceof Object) {
      const objectKeys = Object.keys(user);
      if (!(objectKeys.length && "access" in user)) {
        router.push("/user/login");
      }
    }
    return <Component {...props} />;
  };
  return Auth;
};

export default withPrivateRoute;
