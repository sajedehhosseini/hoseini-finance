import * as yup from "yup";
let requireMesssage = "این فیلد اجباری است.";
let emailMesssage = "فرمت ایمیل صحیح نمی باشد";
export const commonQuestionsSchema = yup.object().shape({
  title: yup.string().required(requireMesssage),
  email: yup.string().required(requireMesssage).email(emailMesssage),
  question: yup.string().required(requireMesssage),
});
//user panel
export const userLoginSchema = yup.object().shape({
  username: yup
    .string()
    .required(requireMesssage)
    .min(3, "نام کاربری حداقل 3 حرف می باشد")
    .max(12, "نام کاربری حداکثر 12 حرف می باشد"),
  password: yup
    .string()
    .required(requireMesssage)
    .matches(/(?=.*[a-z])/, {
      message: "رمزعبور باید حداقل شامل یک حرف کوچک باشد",
      excludeEmptyString: false,
    })
    .matches(/(?=.*[A-Z])/, {
      message: "رمزعبور باید حداقل شامل یک حرف بزرگ باشد",
      excludeEmptyString: false,
    })
    .matches(/(?=.{6,})/, {
      message: "طول رمز عبور باید بیشتر از ۶ کاراکتر باشد",
      excludeEmptyString: false,
    }),
});
export const userRegisterSchema = yup.object().shape({
  name: yup
    .string()
    .required(requireMesssage)
    .min(3, "نام حداقل 3 حرف می باشد")
    .max(20, "نام حداکثر 12 حرف می باشد"),
  username: yup
    .string()
    .required(requireMesssage)
    .min(3, "نام کاربری حداقل 3 حرف می باشد")
    .max(12, "نام کاربری حداکثر 12 حرف می باشد"),
  phone: yup
    .string()
    .required(requireMesssage)
    .matches(/^(0?)9\d{9}$/i, "شماره تلفن را درست وارد کنید"),
  email: yup.string().required(requireMesssage).email(emailMesssage),
  password: yup
    .string()
    .required(" رمز عبور اجباری است.")
    .matches(/(?=.*[a-z])/, {
      message: "رمزعبور باید حداقل شامل یک حرف کوچک باشد",
      excludeEmptyString: false,
    })
    .matches(/(?=.*[A-Z])/, {
      message: "رمزعبور باید حداقل شامل یک حرف بزرگ باشد",
      excludeEmptyString: false,
    })
    .matches(/(?=.{6,})/, {
      message: "طول رمز عبور باید بیشتر از ۶ کاراکتر باشد",
      excludeEmptyString: false,
    }),
  password2: yup
    .string()
    .required("تکرار رمزعبور اجباری است")
    .oneOf([yup.ref("password"), null], "رمز عبور مطابقت ندارد"),
});
export const userForgotPassSchema = yup.object().shape({
  email: yup.string().required(requireMesssage).email(emailMesssage),
});
export const userChangePassSchema = yup.object().shape({
  password: yup
    .string()
    .required(requireMesssage)
    .matches(/(?=.*[a-z])/, {
      message: "رمزعبور باید حداقل شامل یک حرف کوچک باشد",
      excludeEmptyString: false,
    })
    .matches(/(?=.*[A-Z])/, {
      message: "رمزعبور باید حداقل شامل یک حرف بزرگ باشد",
      excludeEmptyString: false,
    })
    .matches(/(?=.{6,})/, {
      message: "طول رمز عبور باید بیشتر از ۶ کاراکتر باشد",
      excludeEmptyString: false,
    }),
  password2: yup
    .string()
    .required(requireMesssage)
    .oneOf([yup.ref("password"), null], "رمز عبور مطابقت ندارد"),
});
export const UpdateInfoSchema = yup.object().shape({
  name: yup
    .string()
    .required(requireMesssage)
    .min(3, "نام حداقل 3 حرف می باشد")
    .max(20, "نام حداکثر 12 حرف می باشد"),
  email: yup.string().required(requireMesssage).email(emailMesssage),
});
export const updatePassSchema = yup.object().shape({
  password: yup
    .string()
    .required(" رمز عبور اجباری است.")
    .matches(/(?=.*[a-z])/, {
      message: "رمزعبور باید حداقل شامل یک حرف کوچک باشد",
      excludeEmptyString: false,
    })
    .matches(/(?=.*[A-Z])/, {
      message: "رمزعبور باید حداقل شامل یک حرف بزرگ باشد",
      excludeEmptyString: false,
    })
    .matches(/(?=.{6,})/, {
      message: "طول رمز عبور باید بیشتر از ۶ کاراکتر باشد",
      excludeEmptyString: false,
    }),
  password2: yup
    .string()
    .required(requireMesssage)
    .oneOf([yup.ref("password"), null], "رمز عبور مطابقت ندارد"),
});
export const ticketChatSchema = yup.object().shape({
  message: yup.string().required(requireMesssage),
});
export const createTicketSchema = yup.object().shape({
  title: yup.string().required(requireMesssage),
  message: yup.string().required(requireMesssage),
});
//admin panel
export const createUserSchema = yup.object().shape({
  name: yup
    .string()
    .required(requireMesssage)
    .min(3, "نام کاربری حداقل 3 حرف می باشد")
    .max(20, "نام کاربری حداکثر 20 حرف می باشد"),
  username: yup
    .string()
    .required(requireMesssage)
    .min(3, "نام کاربری حداقل 3 حرف می باشد")
    .max(20, "نام کاربری حداکثر 20 حرف می باشد"),
  phone: yup
    .string()
    .required(requireMesssage)
    .matches(/^(0?)9\d{9}$/i, "شماره تلفن را درست وارد کنید"),
  email: yup.string().required(requireMesssage).email(emailMesssage),
  password: yup
    .string()
    .required(requireMesssage)
    .matches(/(?=.*[a-z])/, {
      message: "رمزعبور باید حداقل شامل یک حرف کوچک باشد",
      excludeEmptyString: false,
    })
    .matches(/(?=.*[A-Z])/, {
      message: "رمزعبور باید حداقل شامل یک حرف بزرگ باشد",
      excludeEmptyString: false,
    })
    .matches(/(?=.{6,})/, {
      message: "طول رمز عبور باید بیشتر از ۶ کاراکتر باشد",
      excludeEmptyString: false,
    }),
});
