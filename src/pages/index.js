import { useState } from 'react';
import Styles from "../styles/home.module.scss";
import Head from "next/head";
import { useRouter } from "next/router";
import { Container, Row, Col, Card, Button } from "react-bootstrap";
import { Play, Dot, ArrowLeft } from "react-bootstrap-icons";
import { getCoursersList } from "../services";

//components
import GridCourseCard from "../components/course-card/grid-course/GridCourseCard";
import ViewersCard from "../components/viewers/Viewers";

const features = [
  {

    title: "دسترسی سریع",
    icon: "./icons/accessible.png",
  },
  {

    title: "پشتیبانی آنلاین",
    icon: "./icons/headsets-with-microphone.png",
  },
  {

    title: "یادگیری آسان",
    icon: "./icons/idea.png",
  },
  {

    title: "دوره های جامع",
    icon: "./icons/insight.png",
  },
];
const financialSteps = [
  {

    pic: "./icons/transaction.png",
    title: "معامله‌گری در رمزارزها",
    text: "تیم تحقیقات و بررسی ما به صورت روزانه با بررسی فرصت ها، سرفصل های جدید برای دوره های مناسب برای بهبود هرچه بیشتر زندگی مالی شما را تهیه و آماده میکند",
  },
  {

    pic: "./icons/pick.png",
    title: "استخراج رمزارزها",
    text: "تیم تحقیقات و بررسی ما به صورت روزانه با بررسی فرصت ها، سرفصل های جدید برای دوره های مناسب برای بهبود هرچه بیشتر زندگی مالی شما را تهیه و آماده میکند",
  },
  {

    pic: "./icons/euro.png",
    title: "معامله‌گری در بازارهای مالی",
    text: "تیم تحقیقات و بررسی ما به صورت روزانه با بررسی فرصت ها، سرفصل های جدید برای دوره های مناسب برای بهبود هرچه بیشتر زندگی مالی شما را تهیه و آماده میکند",
  },
];
const Home = ({ courses }) => {
  const router = useRouter();
  const [titleLogo, setTitleLogo] = useState({})
  return (
    <>
      <Head>
        <title>Hoseini Finance</title>
        <meta name="viewport" content="initial-scale=1.0, width=device-width" />
      </Head>
      <div id="maincontent" className={`${Styles.homeBanner} w-100 position-relative`}>
        <video
          preload="auto"
          autoPlay
          playsInline
          loop
          muted
          className={Styles.homeVideo}
        >
          <source
            src="http://studio.wdcourse.ir/wp-content/uploads/2020/10/99.mp4"
            type="video/mp4"
          />
        </video>
        <Container className={`${Styles.homeTitle} pt-5`}>
          <div className={`${Styles.text} text-center`}>
            <p className={`${Styles.title} mb-3  f-100 text-center`}>
              حسینی فایننس
            </p>
            <p className={`${Styles.subTitle} text-green f-100 text-center`}>
              آکادمی تخصصی ارزهای دیجیتال
</p>
            <p className="mt-md-3 mt-lg-3 mt-0 small text-center">
              کسب و کار شما می‌تواند رشد کند. درآمد شما می‌تواند چند برابر شود.
              فقط کافی است مسیر درست را بشناسید. ما در این مسیر همراه شما هستیم
            </p>
            <div className="mt-lg-5 mt-md-5 mt-1 d-flex align-items-stretch justify-content-center">
              <button className="text-light btn btn-outline-success btn-md bg-green ml-5">
                بررسی دوره ها
              </button>
              <button className="text-light btn btn-outline-success btn-md">
                {" "}
                پیش نمایش <Play className={Styles.icon} />
              </button>
            </div>
          </div>
        </Container>
      </div>
      {/* <div className={`w-100 ${Styles.features}`}>
        <Container>
          <Row className="justify-content-between align-items-stretch">
            {features.map((feature, index) => {
              return (
                <Col lg={3} md={6} sm={6} className={` ${Styles.featuresCol} mb-3`} key={`feature-${index}`}>
                  <Card className={`${Styles.featuresBox} h-100`}>
                    <Card.Body className={Styles.cardBody}>
                      <div className="d-flex align-items-center justify-content-center flex-wrap">
                        <div className="f-100 text-center">
                          <span className={`${Styles.featuresBg} transition`}>
                            <img src={feature.icon} alt={`${feature.title}`} />
                          </span>
                        </div>
                        <p className={`${Styles.text} mt-3 mb-0 text-center`}>
                          {feature.title}
                        </p>
                      </div>
                    </Card.Body>
                  </Card>
                </Col>
              );
            })}
          </Row>
        </Container>
      </div> */}
      {courses && courses.length ? (
        <div className="w-100 py-10">
          <Container>
            <Row className="justify-content-center my-4">
              <Col xs={12} className={`${Styles.mainTitle} py-3`}>
                <h2 className={`${Styles.title} text-center`}>
                  <span className="font-weight-bold">دوره های فعال</span> در
                  مجموعه
                </h2>
                <p className={`${Styles.text} text-center`}>
                  تیم تحقیقات و بررسی ما به صورت روزانه با بررسی فرصت ها، سرفصل
                  های جدید برای دوره های مناسب برای بهبود هرچه بیشتر زندگی مالی
                  شما را تهیه و آماده میکند
                </p>
              </Col>
            </Row>
            <Row className="justify-content-center ">
              {courses.map((coursor, index) => {
                if (index < 3) {
                  return (
                    <>
                      <Col
                        lg={4}
                        md={6}
                        sm={12}
                        key={`coursor-${index}`}
                        className="my-2"
                      >
                        <GridCourseCard data={coursor} />
                      </Col>
                    </>
                  );
                }
              })}
              {courses && courses.length > 3 ? (
                <a className="mt-5 f-100 transition text-center text-info h5 cursor-pointer" onClick={() => router.push("/courses")} >
                  <ArrowLeft className="ml-3" />
                  نمایش همه دوره ها
                </a>
              ) : null}
            </Row>
          </Container>
        </div>
      ) : null}
      <div className={`${Styles.financialStepsWrapp} w-100 py-10`}>
        <Container>
          <Row className="justify-content-center my-4">
            <Col xs={12} className={`${Styles.mainTitle} py-3`}>
              <h2 className={`${Styles.title} text-center`}>
                <span className="font-weight-bold h2 text-green">انتخاب مسیر</span> در بازارهای مالی
              </h2>
              <p className={`${Styles.text} text-center`}>
                کسب و کار شما می‌تواند رشد کند. درآمد شما می‌تواند چند برابر
                شود. فقط کافی است مسیر درست را بشناسید. ما در این مسیر همراه شما
                هستیم{" "}
              </p>
            </Col>
          </Row>
          <Row className={`${Styles.financialSteps}`}>
            {financialSteps.map((step, index) => {
              return (
                <Col lg={4} md={4} sm={4} className="px-lg-4 px-1 mb-3" key={`step-${index}`}>
                  <div className={`${Styles.item} d-flex justify-content-center flex-wrap`}>
                    <div className={`${Styles.image}`} >
                      <span><img src={step.pic} /></span>
                    </div>
                    <div className={`${Styles.desc}`} >
                      <h2 className={`${Styles.title} text-center font-weight-bold text-green h6`}>{step.title}</h2>
                      <div className={Styles.seprator}></div>
                      <p className={`${Styles.text} mb-0`}>{step.text}</p>
                    </div>
                  </div>
                </Col>
              );
            })}
          </Row>
        </Container>
      </div>
      <div className={`${Styles.webSitesWrapp} w-100 py-10 mt-10`}>
        <Container>
          <Row className="align-items-center justify-content-center">
            <Col lg={4} md={4} sm={12} className={`${Styles.mainTitle} py-3 py-lg-0`}>
              <h3 className={`${Styles.title} font-weight-bold text-right h2`}>
                {
                  titleLogo && Object.keys(titleLogo).length ?
                    <>
                      <span className="text-green">{titleLogo.title}</span>
                      <br /> {titleLogo.subTitle}{" "}
                    </>

                    : <>
                      <span className="text-green">با تمامی بخش‌های</span>
                      <br /> حسینی فایننس{" "}
                      <span className="text-green">آشنا شوید</span>
                    </>
                }

              </h3>
            </Col>
            <Col lg={8} md={8} sm={12} >
              <Row className="align-items-start">
                <Col lg={4} md={4} sm={4} className={Styles.col} onMouseOver={() => setTitleLogo({ title: "ایران چارت", subTitle: "وبلاگ ارزهای مجازی" })} onMouseOut={() => setTitleLogo({})}>
                  <div className={Styles.items}>
                    <div className={`${Styles.img}`}>
                      <img src="./images/iranchart.png" />
                    </div>
                    <div className={`${Styles.text} mt-2 text-center`}>

                    </div>
                  </div>
                </Col>
                <Col lg={4} md={4} sm={4} className={Styles.col} onMouseOver={() => setTitleLogo({ title: "ارز تو ارز", subTitle: "صرافی ارزهای مجازی" })} onMouseOut={() => setTitleLogo({})}>
                  <div className={Styles.items}>
                    <div className={`${Styles.img}`}>
                      <img src="./images/arztoarz.png" />
                    </div>
                    <div className={`${Styles.text} mt-2 text-center`}>

                    </div>
                  </div>
                </Col>
                <Col lg={4} md={4} sm={4} className={Styles.col} onMouseOver={() => setTitleLogo({ title: "ولت سنتر", subTitle: "فروشگاه ولت فیزیکی" })} onMouseOut={() => setTitleLogo({})}>
                  <div className={Styles.items}>
                    <div className={`${Styles.img} `}>
                      <img src="./images/walletcenter.png" />
                    </div>
                    <div className={`${Styles.text} mt-2 text-center`}>

                    </div>
                  </div>
                </Col>

              </Row>
            </Col>
          </Row>
        </Container>
      </div>
      <div className="w-100 py-10">
        <Container>
          <Row className="justify-content-center my-4">
            <Col xs={12} className={`${Styles.mainTitle} py-3`}>
              <h2 className={Styles.title}>
                <span className="font-weight-bold h2">بینندگان دوره ها</span> چه
                می‌گویند؟
              </h2>
            </Col>
          </Row>

          <ViewersCard />

        </Container>
      </div>
      <div className={`${Styles.application} w-100 py-10 mt-10`}>
        <Container>
          <Row className="align-items-center">
            <Col lg={6} md={6} sm={4} className="text-center mb-3">
              <img src="./images/mockup.png" />
            </Col>
            <Col lg={6} md={6} sm={8} className={`${Styles.desc}`}>
              <h3 className={`${Styles.title} mb-3`}>
                <span className="font-weight-bold">دانلود اپلیکیشن</span> حسینی
                فایننس
              </h3>
              <div
                className={`${Styles.text} d-flex align-items-center flex-wrap`}
              >
                <p className="text">
                  در اپلیکیشن ما شما می تونید به راحتی به محتوای دوره ها و تمامی
                  بخش های موجود در سایت دسترسی داشته باشید و همچنین شما فقط در
                  اپلیکیشن ما می تونید با دانلود دوره ها هر وقت که خواستین بدون
                  نیاز به اینترنت به محتوای دوره ها دسترسی داشته باشید و به صورت
                  آفلاین به تماشای دوره ها و روند آموزش بپردازید
                </p>
                <p>
                  <span className="ml-2">
                    <Dot className="text-green icon" />
                    مشاهده آفلاین دوره‌ها
                  </span>
                  <span>
                    <Dot className="text-green icon" />
                    پشتیبانی آنلاین و 24ساعته
                  </span>
                </p>
                <button
                  type="button"
                  className="btn btn-dark btn-sm d-flex align-items-center ml-3"
                >
                  <img
                    className={`${Styles.img} ml-2`}
                    src="./images/googleplay.png"
                  />
                  دانلود از گوگل ‌پلی
                </button>
                <button
                  type="button"
                  className="btn btn-dark btn-sm d-flex align-items-center"
                >
                  <img
                    className={`${Styles.img} ml-2`}
                    src="./images/applestore.png"
                  />
                  دانلود از اپ استور
                </button>
              </div>
            </Col>
          </Row>
        </Container>
      </div>
    </>
  );
};

export default Home;

export async function getServerSideProps() {
  try {
    const { data } = await getCoursersList();
    return {
      props: {
        courses: data.data,
      },
    };
  } catch (error) {
    return {
      props: {
        courses: [],
        error: error.message,
      },
    };
  }
}
