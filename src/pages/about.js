import { useState } from "react";
import Styles from "../styles/About.module.scss";
import { Container, Row, Col, Card } from "react-bootstrap";
import { PlayFill } from "react-bootstrap-icons";
import { getAboutMedias } from "../services/static-api-calls";
import Image from 'next/image'
//components
import HeadingCart from "../components/headingcard/HeadingCard";
import StaticsCards from "../components/aboutme/statics/Statics";
import OurTeam from "../components/aboutme/ourteam/OurTeam";
import SocialCart from "../components/social/Social";
import SwiperComponent from "../components/swiper/Swiper";
import ModalComponent from "../components/modal/Modal";
import VideoPlayerComponent from "../components/video-player/VideoPlayerComp";

const swiperParams = {
  slidesPerView: 3,
  spaceBetween: 10,
  freeMode: true,
  navigation: {
    nextEl: ".swiper-button-next",
    prevEl: ".swiper-button-prev",
  },
  breakpoints: {
    320: {
      slidesPerView: 1,
    },
    768: {
      slidesPerView: 2,
    },
    991: {
      slidesPerView: 3,
    },
  }
};

const About = ({ images, videos }) => {
  const [videoSrc, setVideoSrc] = useState("");
  const [imageSrc, setImageSrc] = useState("");
  const [titleModal, setTitleModal] = useState("");

  return (
    <>
      <ModalComponent
        size="xl"
        title={imageSrc ? null : titleModal}
        show={videoSrc || imageSrc ? true : false}
        setShow={(status) => {
          setVideoSrc(status);
          setImageSrc(status);
        }}
      >
        {videoSrc ? (
          <VideoPlayerComponent src="https://aspb21.cdn.asset.aparat.com/aparat-video/27dccba81bfebdfbc066398773923b6228455332-1080p.mp4?wmsAuthSign=eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ0b2tlbiI6ImRjZmNlYzZiNzVlNTc1NDMzZGNiYjI5YTUwYjcxNzM3IiwiZXhwIjoxNjExNDQzNTQyLCJpc3MiOiJTYWJhIElkZWEgR1NJRyJ9.-Rc5QJotGEz0B552v-Eph4yBM9BuDxTiyOiHPtfJg0Q" />
        ) : imageSrc ? (
          <img src={imageSrc} width="100%" height="100%" alt="" />
        ) : null}
      </ModalComponent>
      <HeadingCart
        className="h-100 p-wide"
        background="MR.jpg"
        title="درباره ما"
        statics={() => <StaticsCards />}
        iconButton={() => <PlayFill />}
      >
        <Row className="h-100 align-items-center align-content-center">
          <Card className={`${Styles.aboutDesc} mb-2`}>
            <Card.Body>
              <p className={Styles.subTitle}>
                حضور تو بازارهای مالی برای شما هم
              </p>
              <span className={Styles.title}>آسونه!</span>
              <div className={Styles.cardDesc}>
                <p>
                  من محمد حسینی هستم. تحلیلگر و معامله گر در بازار های مالی اعم
                  از بازار ارز های دیجیتال و دیگر بازار های جهانی. در مقطع
                  کارشناسی و کارشناسی ارشد فارغ التحصیل رشته مکانیک هستم و در
                  حال حاضر دانشجوی دکتری در رشته مدیرت استراتژیک کسب و کار در
                  دانشگاه شهید بهشتی تهران می باشم. از سال 89 فعالیت خود در
                  زمینه بازارهای مالی را شروع کرده و بسیاری از دوره های مطرح و
                  معتبر داخلی و بین المللی مربوط به بازار های مالی را شرکت کرده
                  ام. اکثر معاملات من در بازارهای مالی بر اساس تحلیل تکنیکال هست
                  و عضو فدراسیون بین المللی تحلیلگران تکنیکال و دارای معتبرترین
                  مدرک حرفه ای بین المللی در حوزه تحلیل تکنیکال (تحلیلگر تکنیکال
                  مالی خبره) هستم.
                </p>
              </div>
            </Card.Body>
          </Card>
          <SocialCart />
        </Row>
      </HeadingCart>

      <div className="py-10 bg-green">
        <Container>
          <Row className={Styles.aboutCard}>
            <Col xs={12} className="pb-6">
              <span className={Styles.title}>درباره من</span>
            </Col>
            <Col lg={6} md={12} sm={12} className="pl-8">
              <p>من محمد حسینی هستم</p>
              <p>
                تحلیلگر و معامله گر در بازار های مالی اعم از بازار ارز های
                دیجیتال و دیگر بازار های جهانی. در مقطع کارشناسی و کارشناسی ارشد
                فارغ التحصیل رشته مکانیک هستم و در حال حاضر دانشجوی دکتری در
                رشته مدیرت استراتژیک کسب و کار در دانشگاه شهید بهشتی تهران می
                باشم. از سال 89 فعالیت خود در زمینه بازارهای مالی را شروع کرده و
                بسیاری از دوره های مطرح و معتبر داخلی و بین المللی مربوط به
                بازار های مالی را شرکت کرده ام
              </p>
              <ul>
                <li>انجام معاملات در بازارهای مالی بر اساس تحلیل تکنیکال</li>
                <li>عضو فدراسیون بین المللی تحلیلگران تکنیکال</li>
                <li>دارای معتبر ترین مدرک حرفه ای بین المللی</li>
              </ul>
            </Col>
            <Col lg={6} md={12} sm={12}>
              <Image src="/images/aboutme.png" width="800" height="400"/>
              {/* <img src="./images/aboutme.png" /> */}
            </Col>
          </Row>
        </Container>
      </div>

      <div className="w-100 py-10">
        <Container>
          <Row
            className={`${Styles.ourTeam} flex-row-reverse align-items-center`}
          >
            <Col item xs={12} className="pb-6">
              <h2 className={Styles.title}>تیم ما</h2>
            </Col>
            {["", "", "", ""].map((item) => {
              return <OurTeam />;
            })}
          </Row>
        </Container>
      </div>
      <div className={`${Styles.galleryContainer} w-100 py-10`}>
        <Container>
          <Row className={Styles.videoGallery}>
            <Col lg={12} className="pb-6">
              <span className={Styles.title}>گالری ویدئو همایش ها</span>
            </Col>
            <Col lg={12}>

              <SwiperComponent params={swiperParams}>
                {videos.map((item) => {
                  return (
                    <div className={`${Styles.box} p-2`}
                      onClick={() => {
                        setVideoSrc(`http://77.237.77.250:8080/${item.video}`);
                        setTitleModal("گالری ویدئو همایش ها");
                      }}
                    >
                      <span className={Styles.icon}><PlayFill /></span>
                      <div className={`${Styles.desc} transition`}>
                        <p className="my-1">همایش درامدزایی از ارزهای دیجیتال</p>
                        <p className={Styles.time}><span className="FaNum">60</span>دقیقه</p>
                      </div>
                      <img src={"/images/are_ySXtZig.jpg"} alt="" />
                    </div>
                  );
                })}
              </SwiperComponent>
            </Col>
          </Row>
        </Container>
      </div>
      <div className={`${Styles.galleryContainer} w-100 py-10 bg-white`}>
        <Container>
          <Row className={Styles.imageGallery}>
            <Col lg={12} className="pb-6">
              <span className={Styles.title}>گالری تصاویر همایش ها</span>
            </Col>
            <Col item xs={12} className="pb-6">

              <SwiperComponent params={swiperParams}>
                {images.map((item) => {
                  return (
                    <div className="p-2 cursor-pointer"
                      key={`image-${item.id}`}
                      onClick={() => setImageSrc(`http://77.237.77.250:8080/${item.image}`)}
                    >
                      <img src={`http://77.237.77.250:8080/${item.image}`} alt="" />
                    </div>
                  );
                })}
              </SwiperComponent>
            </Col>
          </Row>
        </Container> 
      </div>
    </>
  );
};

export async function getServerSideProps() {
  try {
    let { data } = await getAboutMedias();
    const { images, videos } = data.data;
    return {
      props: {
        images,
        videos,
      },
    };
  } catch (error) {
    return {
      props: {
        images: [],
        videos: [],
        error: error.message,
      },
    };
  }
}
export default About;
//sample images data
// id: 3,
// image: '/media/about/image/are_ySXtZig.jpg',
// date: '2019-11-14',
// title: 'alooo021'


//sample videos data
// id: 3,
// icon: '/media/about/video/image/are_m0wyJWw.jpg',
// video: '/media/about/video/video/v4_1_jUP3zIi.mp4',
// date: '2019-11-14',
// title: 'alooo'
