import React, { useState } from "react";
import { Form, Row, Col, Alert } from "react-bootstrap";
import { useRouter } from "next/router";
import Head from "next/head";
import Link from "next/link";
import { registerUser } from "../../services";
import { useForm } from "react-hook-form";
import { yupResolver } from "@hookform/resolvers/yup";
import { userRegisterSchema } from "../../configs/formValidateSchema";
//components
import AuthTemplate from "../../components/templates/auth/AuthTemplate";
import CustomizeInput from "../../components/form/input/Input";
import CustomizeButton from "../../components/form/button/Button";
import {
  Person,
  Lock,
  Telephone,
  Envelope,
  PersonCheck,
} from "react-bootstrap-icons";

const Register = () => {
  const router = useRouter();
  const [isLoading, setIsLoading] = useState(false);
  const [serverError, setServerError] = useState();
  const { handleSubmit, register, errors } = useForm({
    resolver: yupResolver(userRegisterSchema),
  });

  const onRegister = async (vals) => {
    setIsLoading(true);
    try {
      const res = await registerUser(vals);
    } catch (error) {
      console.log("error ----------->", error);
    } finally {
      setIsLoading(false);
    }
  };
  return (
    <>
      <Head>
        <title>Register | ثبت نام</title>
      </Head>
      <AuthTemplate title="ثبت نام در" image="/images/signup-rafiki.png" hiddenHs>
        <Col xs={12}>
          {serverError ? (
            <Alert
              variant="danger"
              onClose={() => setServerError("")}
              dismissible
            >
              {serverError}
            </Alert>
          ) : null}
          <Form className="d-flex flex-wrap row">
            <Col lg={6}>
              <CustomizeInput
                icon={() => <Person />}
                label="نام و نام خانوادگی "
                size="md"
                name="name"
                ref={register()}
                errorMessage={
                  (errors.name && errors.name.message) ||
                  (serverError && serverError.name)
                }
              />
            </Col>
            <Col lg={6}>
              <CustomizeInput
                icon={() => <PersonCheck />}
                label="نام کاربری "
                size="md"
                name="username"
                ref={register()}
                errorMessage={
                  (errors.username && errors.username.message) ||
                  (serverError && serverError.username)
                }
              />
            </Col>
            <Col lg={6}>
              <CustomizeInput
                icon={() => <Telephone />}
                label="شماره موبایل "
                size="md"
                name="phone"
                ref={register()}
                errorMessage={
                  (errors.phone && errors.phone.message) ||
                  (serverError && serverError.phone)
                }
              />
            </Col>
            <Col lg={6}>
              <CustomizeInput
                icon={() => <Envelope />}
                label="ایمیل"
                size="md"
                name="email"
                ref={register()}
                errorMessage={
                  (errors.email && errors.email.message) ||
                  (serverError && serverError.email)
                }
              />
            </Col>
            <Col lg={6}>
              <CustomizeInput
                icon={() => <Lock />}
                label="رمز عبور "
                type="password"
                size="md"
                name="password"
                ref={register()}
                errorMessage={
                  (errors.password && errors.password.message) ||
                  (serverError && serverError.password)
                }
              />
            </Col>
            <Col lg={6}>
              <CustomizeInput
                icon={() => <Lock />}
                label="تکرار رمز عبور "
                type="password"
                size="md"
                name="password2"
                ref={register()}
                errorMessage={
                  (errors.password2 && errors.password2.message) ||
                  (serverError && serverError.password2)
                }
              />
            </Col>
            <Col lg={12}>
              <CustomizeButton
                isFullwidth
                variant="success"
                size="md"
                className="my-3 submit logRegSubmit"
                onClick={handleSubmit(onRegister)}
                isLoading={isLoading}
              >
                ثبت نام
              </CustomizeButton>
            </Col>
          </Form>
          <p className="subText font-weight-bold text-darkBlue mt-2 text mb-0">
            با ثبت نام در حسینی‌فایننس شما شرایط و قوانین حریم خصوصی آن را
            می‌پذیرید.
          </p>
          <p className="text-center text-darkBlue subText mt-2">
            از قبل ثبت نام کرده‌اید؟{`  `}
            <span className="text-primary">
              <Link href="/user/login">
                <a className="text-green">ورود</a>
              </Link>
            </span>
          </p>
        </Col>
      </AuthTemplate>
    </>
  );
};
export default Register;
