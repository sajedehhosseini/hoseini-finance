import { useEffect, useState } from "react";
import Styles from "../../../styles/Ticket.module.scss";
import { Container, Row, Col, Button, Table } from "react-bootstrap";
import { useRouter } from "next/router";
import Link from "next/link";
import { userTicketList } from "../../../services";
import { convertDate } from "../../../configs/utils";
import withPrivateRoute from "../../../configs/withPrivateRoute";

//components
import TitlePanel from "../../../components/title-panel/TitlePanel";

const TicketPage = () => {
  const router = useRouter();
  const [ticketList, setTicketList] = useState([]);
  useEffect(() => {
    getTicketList();
  }, []);
  const getTicketList = async () => {
    try {
      const { data } = await userTicketList();
      setTicketList(data.data);
    } catch (error) {
    } finally {
    }
  };
  return (
    <Container>
      <Row className="justify-content-between align-items-center my-2">
        <Col lg={12}>
          <TitlePanel title="پشتیبانی" />
        </Col>
        <Col
          lg={12}
          className="d-flex justify-content-between align-items-center flex-wrap"
        >
          <span className={`${Styles.descTitle} sm-text`}>
            در صورت بروز مشکل با تیم پشیبانی ما در ارتباط باشید ما در اولین فرصت
            پاسخگوی شما هستیم
          </span>
          <Button
            variant="success"
            size="sm"
            onClick={() => router.push("/user/ticket/create-ticket")}
          >
            ایجاد تیکت
          </Button>
        </Col>
      </Row>
      <Row className="justify-content-center mt-3">
        <Col lg={12}>
          {ticketList && ticketList.length ? (
            <Table
              responsive
              striped
              bordered
              hover
              className={Styles.ticketTable}
            >
              <thead>
                <tr className="text-center">
                  <th>عنوان تیکت</th>
                  <th>تاریخ ایجاد تیکت</th>
                  <th>جزییات</th>
                </tr>
              </thead>
              <tbody>
                {ticketList.map((ticket, index) => {
                  return (
                    <tr key={index}>
                      <td className="text-center">{ticket.title}</td>
                      <td className="FaNum text-center">
                        {convertDate(ticket.created_at, "date")}
                      </td>
                      <td className="text-center">
                        <Link
                          href={`/user/ticket/${ticket.id}?title=${ticket.title}`}
                        >
                          <a className="text-green">جزئیات بیشتر</a>
                        </Link>
                      </td>
                    </tr>
                  );
                })}
              </tbody>
            </Table>
          ) : (
            <div className="w-100 flex-column d-flex align-items-center justify-content-center py-3">
              <span className="mb-5 text-green"> پیامی ثبت نشده است!</span>
              <img src="/images/customer-service.png" />
            </div>
          )}
        </Col>
      </Row>
    </Container>
  );
};

export default withPrivateRoute(TicketPage);

// created_at: "2021-01-23T12:37:06Z"
// id: 27
// status: "در حال بررسی"
// title: "s"
// user: "sajihosseini"
