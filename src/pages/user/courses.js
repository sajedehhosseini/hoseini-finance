import { useEffect, useState } from "react";
import { Container, Row, Col, Card } from "react-bootstrap";
import { userCoursesList } from "../../services";
import Link from "next/link";
import withPrivateRoute from "../../configs/withPrivateRoute";

//components
import RowCourseCard from "../../components/course-card/row-course/RowCourseCard";
import CustomizeButton from "../../components/form/button/Button";
import TitlePanel from "../../components/title-panel/TitlePanel";

const Coursers = () => {
  const [courseList, setCourseList] = useState([]);
  useEffect(() => {
    getCourses();
  }, []);
  const getCourses = async () => {
    try {
      const { status, data } = await userCoursesList();
      if (status === 200) {
        setCourseList(data.data);
      }
    } catch (error) {
      console.log(error);
    } finally {
    }
  };
  return (
    <Container>
      <Row>
        <Col lg={12}>
          <TitlePanel title="دوره‌های خریداری شده" />
        </Col>
        <Col lg={12}>
          <ul>
            {courseList && courseList.length ? (
              courseList.map((course, index) => {
                return (
                  <li key={index}>
                    <RowCourseCard data={course} />
                  </li>
                );
              })
            ) : (
              <div className="w-100 flex-column d-flex align-items-center justify-content-center py-3">
                <span className="mb-5 text-green"> دوره خریداری نشده است !</span>
                <img src="/images/customer-service.png" />
              </div>
            )}
          </ul>
        </Col>
      </Row>
      <Row>
        <Col className="text-center mx-5">
          <Link href="/courses">
            <a>
              <span className="mb-5">خرید دوره جدید</span>
            </a>
          </Link>
        </Col>
      </Row>
    </Container>
  );
};

export default withPrivateRoute(Coursers);
