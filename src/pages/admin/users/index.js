import { useEffect, useState } from "react";
import Link from "next/link";
import { Container, Row, Col, Table, Button } from "react-bootstrap";
import { users, deleteUser } from "../../../services";
//components
import CustomizeInput from "../../../components/form/input/Input";
import Pagination from "../../../components/paigination/Pagination";
const User = () => {
  const [list, setList] = useState([]);
  const [searchBox, setSearchBox] = useState("");
  const [pageCount, setPageCount] = useState(1);
  useEffect(() => {
    getList();
  }, []);
  const getList = async () => {
    try {
      const { data } = await users();
      setList(data.data);
      setPageCount(data.page_count);
    } catch (error) {
    } finally {
    }
  };

  const delUserFunc = async (id) => {
    try {
      const { status } = await deleteUser(id);
    } catch (error) {
    } finally {
    }
  };
  // const updateUserFunc = async (id,vals) => {
  // 	try {
  // 		const {status} = await ?(id,vals);
  // 	} catch (error) {
  // 	} finally {
  // 	}
  // };
  return (
    <Container>
      <Row className="mb-6 align-items-center justify-content-between">
        <Col>
          <CustomizeInput onChange={(val) => setSearchBox(val.target.value)} />
        </Col>
        <Col className="text-left">
          <Link href="/admin/users/add-user">
            <a>
              <Button variant="success">افزودن</Button>
            </a>
          </Link>
        </Col>
      </Row>
      <Row>
        <Col>
          <Table responsive="xl">
            <thead>
              <tr>
                <th>#</th>
                <th>نام</th>
                <th>موبایل</th>
                <th>نام کاربری</th>
                <th>ایمیل</th>
                <th></th>
                <th></th>
              </tr>
            </thead>
            <tbody>
              {list
                .filter(
                  (item) =>
                    item.name.includes(searchBox) ||
                    item.username.includes(searchBox)
                )
                .map((item, index) => {
                  return (
                    <tr key={item.id}>
                      <td>{index + 1}</td>
                      <td> {item.name} </td>
                      <td> {item.phone} </td>
                      <td> {item.username} </td>
                      <td> {item.email} </td>
                      <td>
                        <Button variant="link">تغییر رمز</Button>
                      </td>
                      <td>
                        <Button
                          variant="link"
                          onClick={() => delUserFunc(item.id)}
                        >
                          حذف
                        </Button>
                      </td>
                    </tr>
                  );
                })}
            </tbody>
          </Table>
          <Pagination
            totalRecords={pageCount * 20}
            pageLimit={20}
            pageNeighbours={2}
            handleChangePage={(value) => console.log(value)}
          />
        </Col>
      </Row>
    </Container>
  );
};
export default User;

// email: "alirezaaaa@yahoo.com"
// home_number: "0"
// id: 1
// image: "/media/profile2.jpg"
// name: "محمود سفیری"
// phone: "09366915446"
// products: [
// 	{
// 		authors: []
// 		category: 2
// 		created_at: "2019-09-03T22:52:53Z"
// 		desc: ""
// 		desc_sm: "آموزش صفر تا صد تریدینگ، تحلیل تکنیکال ، ماینینگ و روش های کسب درامد از بازار های ارز دیجیتال"
// 		file: "/media/files/mlUmP.jpg"
// 		id: 3
// 		image: "/media/images/oT4AI.jpg"
// 		level: "مبتدی تا پیشرفته"
// 		off_time: ""
// 		old_price: 0
// 		price: 3980000
// 		title: "دوره جامع ارز های دیجیتال"
// 		type: "فیلم آموزشی"
// 		updated_at: "2020-10-10T00:32:06Z"
// 	},
// ]
// username: "opeth"
