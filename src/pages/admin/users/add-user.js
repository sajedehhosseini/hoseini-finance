import { useState } from "react";
import { Container, Row, Col, Form } from "react-bootstrap";
import { useForm } from "react-hook-form";
import { yupResolver } from "@hookform/resolvers/yup";
import { createUserSchema } from "../../../configs/formValidateSchema";
import { addUser } from "../../../services";
//components
import CustomizeInput from "../../../components/form/input/Input";
import CustomizeButton from "../../../components/form/button/Button";
import CustomizeSelect from "../../../components/form/select/Select";
import CustomizeAlert from "../../../components/form/alert/Alert";

const CreateNewِUser = () => {
  const [isLoading, setIsLoading] = useState(false);
  const [serverError, setServerError] = useState();
  const [serverMessage, setServerMessage] = useState("");
  const { handleSubmit, register, errors } = useForm({
    resolver: yupResolver(createUserSchema),
  });
  const createNewUser = async (vals) => {
    vals = { ...vals, password2: vals.password };
    try {
      const { status, data } = await addUser(vals);
      if (status === 201) {
        setServerMessage(data.message);
      }
    } catch (error) {
      if (error.response) {
        setServerError(error.response.data.message);
      }
    } finally {
    }
  };
  return (
    <Container>
      <Form className="my-5">
        {serverMessage ? (
          <CustomizeAlert
            variant="success"
            onClose={() => setServerMessage("")}
            dismissible
          >
            {serverMessage}
          </CustomizeAlert>
        ) : null}
        <Row>
          <Col lg={6}>
            <CustomizeInput
              label="نام"
              name="name"
              ref={register()}
              errorMessage={
                (errors.name && errors.name.message) ||
                (serverError && serverError.name)
              }
            />
          </Col>
          <Col lg={6}>
            <CustomizeInput
              label="شماره تماس"
              name="phone"
              ref={register()}
              errorMessage={
                (errors.phone && errors.phone.message) ||
                (serverError && serverError.phone)
              }
            />
          </Col>
          <Col lg={6}>
            <CustomizeInput
              type="email"
              label="ایمیل"
              name="email"
              ref={register()}
              errorMessage={
                (errors.email && errors.email.message) ||
                (serverError && serverError.email)
              }
            />
          </Col>

          <Col lg={6}>
            <CustomizeInput
              label="نام کاربری"
              name="username"
              ref={register()}
              errorMessage={
                (errors.username && errors.username.message) ||
                (serverError && serverError.username)
              }
            />
          </Col>
          <Col lg={6}>
            <CustomizeInput
              type="password"
              label="رمز عبور"
              name="password"
              ref={register()}
              errorMessage={
                (errors.password && errors.password.message) ||
                (serverError && serverError.password)
              }
            />
          </Col>
          <Col lg={12} className="my-3">
            <CustomizeButton isFullwidth onClick={handleSubmit(createNewUser)}>
              افزودن کاربر
            </CustomizeButton>
          </Col>
        </Row>
      </Form>
    </Container>
  );
};
export default CreateNewِUser;
