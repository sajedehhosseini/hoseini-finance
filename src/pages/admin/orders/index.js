import { useEffect, useState } from 'react';
import { factors } from '../../../services';
import { Container, Row, Col, Table, Button } from 'react-bootstrap';

const Orders = () => {
	const [list, setList] = useState([]);
	useEffect(() => {
		getList();
	}, []);
	const getList = async () => {
		try {
			const { data } = await factors();
		} catch (error) {
		} finally {
		}
	};
	// const addFactorFunc = async (vals) => {
	// 	try {
	// 		const {status} = await ?(vals);
	// 	} catch (error) {
	// 	} finally {
	// 	}
	// };
	// const delFactorFunc = async (id) => {
	// 	try {
	// 		const {status} = await ?(id);
	// 	} catch (error) {
	// 	} finally {
	// 	}
	// };
	// const updateFactorFunc = async (id,vals) => {
	// 	try {
	// 		const {status} = await ?(id,vals);
	// 	} catch (error) {
	// 	} finally {
	// 	}
	// };
	return (
		<Container>
			<Row className="mb-6 align-items-center justify-content-between">
				<Col>search</Col>
			</Row>
			<Row>
				<Col>
					<Table responsive="xl">
						<thead>
							<tr>
								<th>#</th>
								<th>وضعیت</th>
								<th>کد پیگیری</th>
								<th>کد پیگیری</th>
								<th>تاریخ</th>
								<th>دوره</th>
								<th>کاربر</th>
								<th>شماره تماس</th>
								<th>مبلغ</th>
							</tr>
						</thead>
						<tbody>
							{['', '']
								.filter((item) => item)
								.map((item, index) => {
									return (
										<tr key={item.id}>
											<td>{index + 1}</td>
											<td> --- </td>
											<td> --- </td>
											<td> --- </td>
											<td> --- </td>
											<td> --- </td>
											<td> --- </td>
											<td> --- </td>
											<td> --- </td>
										</tr>
									);
								})}
						</tbody>
					</Table>
				</Col>
			</Row>
		</Container>
	);
};
export default Orders;

// created_at: "2019-09-27T22:17:52Z"
// done: 0
// factor_api: "aFqDRzfiqFkQ7JqyyixuoFav1jltA65hGnhZm8cRcmf02OYbzW"
// finish_date: null
// finished: false
// id: 51196
// is_manual: false
// off_code: null
// off_price: 0
// price: 1290000
// tracking_code: null
// updated_at: "2020-01-01T12:24:27Z"
// product: {
// 	authors: []
// 	category: 2
// 	created_at: "2019-09-03T22:52:53Z",
// 	desc:'',
// 	desc_sm: "آموزش صفر تا صد تریدینگ، تحلیل تکنیکال ، ماینینگ و روش های کسب درامد از بازار های ارز دیجیتال"
// 	file: "/media/files/mlUmP.jpg"
// 	id: 3
// 	image: "/media/images/oT4AI.jpg"
// 	level: "مبتدی تا پیشرفته"
// 	off_time: ""
// 	old_price: 0
// 	price: 3980000
// 	title: "دوره جامع ارز های دیجیتال"
// 	type: "فیلم آموزشی"
// 	updated_at: "2020-10-10T00:32:06Z"
// }
