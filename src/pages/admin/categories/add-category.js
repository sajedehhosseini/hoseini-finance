import { useState } from "react";
import { Container, Row, Col, Form } from "react-bootstrap";
import { useForm } from "react-hook-form";
import { yupResolver } from "@hookform/resolvers/yup";
// import {} from "../../../configs/adminFormValidationSchema";
// import {} from '../../../services';
//components
import CustomizeInput from "../../../components/form/input/Input";
import CustomizeButton from "../../../components/form/button/Button";
import CustomizeSelect from "../../../components/form/select/Select";

const CreateNewCategory = () => {
  const [isLoading, setIsLoading] = useState(false);
  const { handleSubmit, register, errors } = useForm({
    // resolver:yupResolver()
  });
  const createNewCategory = async (vals) => {
    console.log(vals)
    try {
    } catch (error) {
    } finally {
    }
  };
  return (
    <Container>
      <Form className="my-5">
        <Row>
          <Col lg={6}>
            <CustomizeSelect
              label="دسته والد"
              options={["بورس", "ارز دیجیتال"]}
            />
          </Col>
          <Col lg={6}>
            <CustomizeInput label="نام دسته" 
            name="title"
            ref={register()}/>
          </Col>
          <Col lg={12}>
            <CustomizeInput rows={3} label="توضیحات دسته" 
            name="desc"
            ref={register()}/>
          </Col>
          <Col lg={12} className="my-3">
            <CustomizeButton isFullwidth outlined>
              افزودن تصویر دسته
            </CustomizeButton>
          </Col>
          <Col lg={12} className="my-3">
            <CustomizeButton
              isFullwidth
              onClick={handleSubmit(createNewCategory)}
            >
              افزودن دسته{" "}
            </CustomizeButton>
          </Col>
        </Row>
      </Form>
    </Container>
  );
};
export default CreateNewCategory;
