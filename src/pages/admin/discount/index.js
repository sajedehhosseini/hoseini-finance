import { useEffect, useState } from "react";
import { Container, Row, Col, Table, Button } from "react-bootstrap";
import Link from "next/link";
import { discounts } from "../../../services";

const Discount = () => {
  const [list, setList] = useState([]);
  useEffect(() => {
    getList();
  }, []);
  const getList = async () => {
    try {
      const { data } = await discounts();
      setList(data.data);
    } catch (error) {
    } finally {
    }
  };
  // const addDiscountFunc = async (vals) => {
  // 	try {
  // 		const {status} = await ?(vals);
  // 	} catch (error) {
  // 	} finally {
  // 	}
  // };
  return (
    <Container>
      <Row className="mb-6 align-items-center justify-content-between">
        <Col>search</Col>
        <Col className="text-left">
          <Link href="/admin/discount/add-discount">
            <a>
              <Button variant="success">افزودن</Button>
            </a>
          </Link>
        </Col>
      </Row>
      <Row>
        <Col>
          <Table responsive="xl">
            <thead>
              <tr>
                <th>#</th>
                <th>عنوان</th>
                <th>مقدار</th>
                <th>تعداد</th>
                <th></th>
                <th></th>
              </tr>
            </thead>
            <tbody>
              {list
                .filter((item) => item)
                .map((item, index) => {
                  return (
                    <tr key={item.id}>
                      <td>{index + 1}</td>
                      <td> {item.title} </td>
                      <td> {item.percent} </td>
                      <td> --- </td>
                      <td>
                        <Button variant="link">مشاهده</Button>
                      </td>
                      <td>
                        <Button variant="link">حذف</Button>
                      </td>
                    </tr>
                  );
                })}
            </tbody>
          </Table>
        </Col>
      </Row>
    </Container>
  );
};
export default Discount;

// code: "x14y7"
// created_at: "2021-01-06T15:41:56Z"
// expired_at: "2020-06-13T10:35:07Z"
// id: 13
// limit: 1
// percent: 15
// product: {
// 	authors: []
// 	category: 2
// 	created_at: "2019-09-03T22:52:53Z"
// 	desc: " ",
// 	desc_sm: "آموزش صفر تا صد تریدینگ، تحلیل تکنیکال ، ماینینگ و روش های کسب درامد از بازار های ارز دیجیتال"
// 	file: "/media/files/mlUmP.jpg"
// 	id: 3
// 	image: "/media/images/oT4AI.jpg"
// 	level: "مبتدی تا پیشرفته"
// 	off_time: ""
// 	old_price: 0
// 	price: 3980000
// 	title: "دوره جامع ارز های دیجیتال"
// 	type: "فیلم آموزشی"
// 	updated_at: "2020-10-10T00:32:06Z"
// }
// start_at: "2020-06-13T10:35:07Z"
// title: "mmmd44"
// updated_at: "2021-01-06T15:41:56Z"
