import { useState } from "react";
import { Container, Row, Col, Form } from "react-bootstrap";
import { useForm } from "react-hook-form";
import { yupResolver } from "@hookform/resolvers/yup";
// import {} from "../../../configs/adminFormValidationSchema";
// import {} from '../../../services';
//components
import CustomizeInput from "../../../components/form/input/Input";
import CustomizeButton from "../../../components/form/button/Button";
import CustomizeSelect from "../../../components/form/select/Select";

const CreateNewِDiscount = () => {
  const [isLoading, setIsLoading] = useState(false);
  const { handleSubmit, register, errors } = useForm({
    // resolver:yupResolver()
  });
  const createNewDiscount = async () => {
    try {
    } catch (error) {
    } finally {
    }
  };
  return (
    <Container>
      <Form className="my-5">
        <Row>
          <Col lg={12}>
            <CustomizeInput
              label="عنوان دوره"
              name=""
              ref={register()}
              // errorMessage={
              //   (errors.category && errors.category.message) ||
              //   (serverError && serverError.category)
              // }
            />
          </Col>
          <Col lg={6}>
            <CustomizeSelect
              label="محدودیت"
              options={[]}
              name=""
              ref={register()}
              // errorMessage={
              //   (errors.category && errors.category.message) ||
              //   (serverError && serverError.category)
              // }
            />
          </Col>
          <Col lg={6}>
            <CustomizeSelect
              label="نوع تخفیف"
              options={[]}
              name=""
              ref={register()}
              // errorMessage={
              //   (errors.category && errors.category.message) ||
              //   (serverError && serverError.category)
              // }
            />
          </Col>

          <Col lg={6}>
            <CustomizeInput
              type="number"
              label="میزان تخفیف"
              name="percent"
              ref={register()}
              // errorMessage={
              //   (errors.category && errors.category.message) ||
              //   (serverError && serverError.category)
              // }
            />
          </Col>
          <Col lg={6}>
            <CustomizeInput
              type="number"
              label="تعداد"
              name=""
              ref={register()}
              // errorMessage={
              //   (errors.category && errors.category.message) ||
              //   (serverError && serverError.category)
              // }
            />
          </Col>
          <Col lg={6}>
            <CustomizeInput
              label="تاریخ شروع تخفیف"
              name="start_at"
              ref={register()}
              // errorMessage={
              //   (errors.category && errors.category.message) ||
              //   (serverError && serverError.category)
              // }
            />
          </Col>
          <Col lg={6}>
            <CustomizeInput
              label="تاریخ اتمام تخفیف"
              name="expired_at"
              ref={register()}
              // errorMessage={
              //   (errors.category && errors.category.message) ||
              //   (serverError && serverError.category)
              // }
            />
          </Col>

          <Col lg={6}>
            <CustomizeSelect
              label="فرمت کد"
              options={[]}
              name=""
              ref={register()}
              // errorMessage={
              //   (errors.category && errors.category.message) ||
              //   (serverError && serverError.category)
              // }
            />
          </Col>
          <Col lg={6}>
            <CustomizeInput
              label="کد"
              name="code"
              ref={register()}
              // errorMessage={
              //   (errors.category && errors.category.message) ||
              //   (serverError && serverError.category)
              // }
            />
          </Col>

          <Col lg={12} className="my-3">
            <CustomizeButton
              isFullwidth
              onClick={handleSubmit(createNewDiscount)}
            >
              افزودن تخفیف
            </CustomizeButton>
          </Col>
        </Row>
      </Form>
    </Container>
  );
};
export default CreateNewِDiscount;

// {"title":"mmmd44",
//  "":"2020-06-13T10:35:07Z",
//   "":"x14y7", "product":3}
