import { useState } from "react";
import { Container, Row, Col, Form } from "react-bootstrap";
import { useForm } from "react-hook-form";
import { yupResolver } from "@hookform/resolvers/yup";
// import {} from "../../../configs/adminFormValidationSchema";
import { addCourse } from "../../../services";
//components
import CustomizeInput from "../../../components/form/input/Input";
import CustomizeButton from "../../../components/form/button/Button";
import CustomizeSelect from "../../../components/form/select/Select";

const CreateNewCourser = () => {
  const [isLoading, setIsLoading] = useState(false);
  const [serverError, setServerError] = useState();
  const { handleSubmit, register, errors } = useForm({
    // resolver:yupResolver()
  });
  const formData = new FormData();
  const createNewCourse = async (vals) => {
    for(let key in vals){
      console.log(key,vals[key]);
      formData.append(key,vals[key]);
    }
    try {
      // const res = await addCourse();
    } catch (error) {
      console.log(error);
    } finally {
    }
  };
  return (
    <Container>
      <Form className="my-5">
        <Row>
          <Col lg={6}>
            <CustomizeSelect
              label="دسته بندی"
              options={["بورس", "ارز دیجیتال"]}
              name="category"
              ref={register()}
              errorMessage={
                (errors.category && errors.category.message) ||
                (serverError && serverError.category)
              }
            />
          </Col>
          <Col lg={6}>
            <CustomizeSelect
              label="سطح دوره"
              options={["مبتدی تا پیشرفته", "مبتدی", "متوسط", "پیشرفته"]}
              name="level"
              ref={register()}
              errorMessage={
                (errors.level && errors.level.message) ||
                (serverError && serverError.level)
              }
            />
          </Col>
          <Col lg={6}>
            <CustomizeSelect
              label="نوع دوره"
              options={["فیلم آموزشی", "کتاب"]}
              name="type"
              ref={register()}
              errorMessage={
                (errors.type && errors.type.message) ||
                (serverError && serverError.type)
              }
            />
          </Col>
          <Col lg={6}>
            <CustomizeInput
              type="number"
              label="قیمت دوره"
              name="old_price"
              ref={register()}
              errorMessage={
                (errors.old_price && errors.old_price.message) ||
                (serverError && serverError.old_price)
              }
            />
          </Col>
          <Col lg={6}>
            <CustomizeInput
              type="number"
              label="قیمت پس از تخفیف دوره"
              name="price"
              ref={register()}
              errorMessage={
                (errors.price && errors.price.message) ||
                (serverError && serverError.price)
              }
            />
          </Col>
          <Col lg={6}>
            <CustomizeInput
              label="تاریخ اتمام تخفیف"
              name=""
              ref={register()}
              errorMessage={
                (errors.username && errors.username.message) ||
                (serverError && serverError.username)
              }
            />
          </Col>
          <Col lg={12}>
            <CustomizeInput label="عنوان دوره" ref={register()} name="" />
          </Col>
          <Col lg={12}>
            <CustomizeInput
              rows={3}
              label="توضیح مختصر"
              name="desc_sm"
              ref={register()}
              errorMessage={
                (errors.desc_sm && errors.desc_sm.message) ||
                (serverError && serverError.desc_sm)
              }
            />
          </Col>
          <Col lg={12}>
            <CustomizeInput rows={8} label="شرح" 
             name="desc" 
             ref={register()}
             errorMessage={
              (errors.desc && errors.desc.message) ||
              (serverError && serverError.desc)
            }/>
          </Col>
          <Col lg={12} className="my-3">
            <CustomizeButton isFullwidth outlined>
              افزودن تصویر دوره
            </CustomizeButton>
          </Col>
          <Col lg={12} className="my-3">
            <CustomizeButton isFullwidth
            onClick={handleSubmit(createNewCourse)}>افزودن دوره</CustomizeButton>
          </Col>
        </Row>
      </Form>
    </Container>
  );
};
export default CreateNewCourser;
