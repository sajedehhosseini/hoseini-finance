import { useEffect, useState } from "react";
import { Container, Row, Col, Table, Button } from "react-bootstrap";
import Link from "next/link";
import { coursesList } from "../../../services";
const Coursers = () => {
  const [list, setList] = useState([]);
  useEffect(() => {
    getList();
  }, []);
  const getList = async () => {
    try {
      const { data } = await coursesList();
      setList(data.data);
    } catch (error) {
    } finally {
    }
  };
  // const addCourserFunc = async (vals) => {
  // 	try {
  // 		const {status} = await ?(vals);
  // 	} catch (error) {
  // 	} finally {
  // 	}
  // };
  // const updateCourserFunc = async (id,vals) => {
  // 	try {
  // 		const {status} = await ?(id,vals);
  // 	} catch (error) {
  // 	} finally {
  // 	}
  // };
  // const delCourserFunc = async (id) => {
  // 	try {
  // 		const {status} = await ?(id);
  // 	} catch (error) {
  // 	} finally {
  // 	}
  // };
  return (
    <Container>
      <Row className="mb-6 align-items-center justify-content-between">
        <Col>search</Col>
        <Col className="text-left">
          <Link href="/admin/courses/add-course">
            <a>
              <Button variant="success">افزودن دوره</Button>
            </a>
          </Link>
        </Col>
      </Row>
      <Row>
        <Col>
          <Table responsive="xl">
            <thead>
              <tr>
                <th>#</th>
                <th>تصویر</th>
                <th>عنوان</th>
                <th>سطح</th>
                <th>نوع دوره</th>
                <th>قیمت</th>
                <th>دسته بندی</th>
                <th>فایل ها</th>
                <th>کاربران مجاز</th>
                <th></th>
                <th></th>
              </tr>
            </thead>
            <tbody>
              {list
                .filter((item) => item)
                .map((item, index) => {
                  return (
                    <tr key={item.id}>
                      <td>{index + 1}</td>
                      <td>{/* <img src={`/${item.image}`} alt=""/> */}</td>
                      <td>{item.title}</td>
                      <td>{item.level}</td>
                      <td>{item.type}</td>
                      <td>{item.price}</td>
                      <td>{item.category}</td>
                      <td> --- </td>
                      <td> --- </td>
                      <td>
                        <Button variant="link">ویرایش</Button>
                      </td>
                      <td>
                        <Button variant="link">حذف</Button>
                      </td>
                    </tr>
                  );
                })}
            </tbody>
          </Table>
        </Col>
      </Row>
    </Container>
  );
};
export default Coursers;

{
  /* <td>#</td>
		<td>تصویر</td>
		<td>عنوان</td>
		<td>سطح</td>
		<td>نوع دوره</td>
		<td>قیمت</td>
		<td>دسته بندی</td>
		<td>فایل ها</td>
		<td>کاربران مجاز</td>
		<td>ویرایش</td>
		<td>حذف</td> */
}

// authors: [{id: 1, name: "ali", bio: "wowowowowowo", role: "sadsfdgfhgfd"}]
// 		0: {id: 1, name: "ali", bio: "wowowowowowo", role: "sadsfdgfhgfd"}
// 			bio: "wowowowowowo"
// 			id: 1
// 			name: "ali"
// 			role: "sadsfdgfhgfd"
// category: "دسته بندی 2"
// created_at: "2019-09-02T14:28:10Z"
// desc: "aspoihyasgvoashfiug"
// desc_sm: "hassanasdsdsad"
// file: "/media/productfile/1/1_2021-01-06_153644.480787.jpg"
// id: 1
// image: "/media/productimage/are1_B418KJq.jpg"
// level: "ziad"
// off_time: ""
// old_price: 1500
// price: 14000
// title: "asdadsaasd"
// type: "فیلم آموزشی"
// updated_at: "2021-01-06T15:36:44Z"
