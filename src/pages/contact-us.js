import React from 'react';
import HeadingCard from '../components/headingcard/HeadingCard';
import { Row, Col } from 'react-bootstrap';
import Map from '../components/map/Map';
import Styles from "../styles/contact.module.scss";
import { GeoAltFill } from 'react-bootstrap-icons';
import SocialCart from '../components/social/Social';
const ContactUs = () => {
  return (
    <HeadingCard title="تماس با ما" background="contacttus.jpg" iconButton={() => <GeoAltFill />}>
      <Row className={`${Styles.contactBox} h-100 justify-content-center  align-items-center align-content-center `}>
        <Col lg={4} md={12} sm={12}  className="pr-5 py-3">
          <Row>
            <Col xs={12}>
              <h6 className="text-green mb-3 font-weight-bold">ساعت کاری</h6>
              <ul className={Styles.contactInfo}>
                <li>
                  <p>شنبه تا چهارشنبه :</p>
                  <span>9 صبح - 5 بعد از ظهر</span>
                </li>
                <li className="mt-3">
                  <p>پنجشنبه:</p>
                  <span>9 صبح - 2 بعد از ظهر</span>
                </li>

              </ul>
            </Col>
            <Col xs={12} className="mt-3">
              <h6 className="text-green mb-3 font-weight-bold">آدرس</h6>
              <p>
                تهران، خیابان مرزداران، خیابان ابراهیمی، برج الوند، طبقه دوازدهم، واحد ۱۲۰۱
          </p>
            </Col>
          </Row>
        </Col>
        <Col lg={8} md={12} sm={12} className={`${Styles.mapContact} py-3`}>
          <Map />
        </Col>
        <SocialCart />
      </Row>
    </HeadingCard>
  );
};

export default ContactUs;
