import Styles from "../styles/404.module.scss";
import {ChevronLeft } from "react-bootstrap-icons";

const Custom404Page = () => {
    return (
        <>
            <div className={Styles.background}>
                <div className={Styles.errorBox}>
                    <div className={Styles.header}>
                        <h1>404</h1>
                        <p className="mb-0">Page Not 
                        <br/>
                        <span className={Styles.green}>Found</span>
                        </p>
                    </div>
                    <div className={Styles.body}>
                        <p>متاسفانه صفحه مورد نظر شما یافت نشد !</p>
                        <a href="/">بازگشت به سایت <span className={Styles.icon}><ChevronLeft/></span></a>
                    </div>

                </div>
            </div>
        </>
    )
}
export default Custom404Page;