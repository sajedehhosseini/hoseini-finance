import { useState, useEffect } from "react";
import Styles from "../styles/faq.module.scss";
import { Row, Col, Form, Container } from "react-bootstrap";
import {
  ArrowLeftShort,
  ChevronDown,
  ChevronUp,
  ChatSquareDots,
  Envelope,
  Pencil,
  Search,
  Check2Circle,
} from "react-bootstrap-icons";
import { useForm } from "react-hook-form";
import { yupResolver } from "@hookform/resolvers/yup";
import { commonQuestionsSchema } from "../configs/formValidateSchema";
import { sendQuestions, getQuestionsList } from "../services";
//components
import HeadingCard from "../components/headingcard/HeadingCard";
import CustomizeInput from "../components/form/input/Input";
import CustomizeButton from "../components/form/button/Button";
import Alert from "../components/form/alert/Alert";
import Pagination from "../components/paigination/Pagination";
import TitlePanel from "../components/title-panel/TitlePanel";

const Faq = ({ questions, error }) => {
  const [isLoading, setIsLoading] = useState(false);
  const [formNotif, setFormNotif] = useState("");
  const [serverError, setServerError] = useState();
  const [search, setSearch] = useState("");
  const [filterCategory, setFilterCategory] = useState("");
  const [categoryList, setCategoryList] = useState([]);
  const [currentPageQuestions, setCurrentPageQuestions] = useState(0);
  const [activeQuestionId, setActiveQuestionId] = useState(0);
  const { handleSubmit, register, errors } = useForm({
    resolver: yupResolver(commonQuestionsSchema),
  });
  useEffect(() => {
    if (questions && questions.length) {
      const arr = [];
      for (let question of questions) {
        if (!arr.includes(question.title)) {
          arr.push(question.title);
        }
      }
      setCategoryList(arr);
    }
  }, []);
  const onSendQuestion = async (vals) => {
    setIsLoading(true);
    try {
      const { status } = await sendQuestions(vals);
      if (status === 201) {
        setFormNotif("پیام شما با موفقیت ارسال شد.");
      }
    } catch (error) {
      console.log(error);
    } finally {
      setIsLoading(false);
    }
  };

  return (
    <>
      <HeadingCard title="پرسش‌های متداول" background="faqs.jpg">
        <Row
          className={`${Styles.faqBox} h-100 justify-content-center  align-items-center`}
        >
          <Col xs={10}>
            {formNotif ? (
              <Alert
                variant="success"
                dismissible
                onClose={() => setFormNotif("")}
              >
                {formNotif}
              </Alert>
            ) : null}
            <h5 className="text-success text-center mb-3">
              سوالی دارید از ما بپرسید
            </h5>
            <p className="text-justify text">
              لورم ایپسوم متن ساختگی با تولید سادگی نامفهوم از صنعت چاپ، و با
              استفاده از طراحان گرافیک است، چاپگرها و متون بلکه روزنامه و مجله .{" "}
            </p>
            <Form className="row">
              <Col className="px-2" lg={6}>
                <CustomizeInput
                  icon={() => <Pencil />}
                  label="عنوان"
                  name="title"
                  ref={register()}
                  size="md"
                  errorMessage={
                    (errors.title && errors.title.message) ||
                    (serverError && serverError.title)
                  }
                />
              </Col>
              <Col className="px-2" lg={6}>
                <CustomizeInput
                  type="email"
                  icon={() => <Envelope />}
                  label="ایمیل"
                  size="md"
                  name="email"
                  ref={register()}
                  errorMessage={
                    (errors.email && errors.email.message) ||
                    (serverError && serverError.email)
                  }
                />
              </Col>
              <Col className="px-2" lg={12}>
                <CustomizeInput
                  icon={() => <ChatSquareDots />}
                  label="متن پیام"
                  name="question"
                  ref={register()}
                  size="md"
                  rows={4}
                  errorMessage={
                    (errors.question && errors.question.message) ||
                    (serverError && serverError.question)
                  }
                />
              </Col>
              <Col className="px-2" lg={12}>
                <CustomizeButton
                  isFullwidth
                  size="md"
                  className="my-3"
                  onClick={handleSubmit(onSendQuestion)}
                  isLoading={isLoading}
                >
                  ارسال پیام
                </CustomizeButton>
              </Col>
            </Form>
          </Col>
        </Row>
      </HeadingCard>
      <Container>
        <Row className="mt-5 mb-3">
          <Col>
            <h5 className="text-success mb-3">پرسش های متداول</h5>
            <p className="text-justify px-3 text">
              اگر پرسش بی‌پاسخی درباره‌ی دوره‌های حسینی فایننس در ذهن دارید،
              می‌توانید روی سوال مورد نظر کلیک کنید تا پاسخ آن را مشاهده نمایید.
            </p>
          </Col>
        </Row>
        <Row className="mb-6">
          <Col lg={3} md={3} sm={12} className="">
            <TitlePanel title="دسته‌بندی ها" />
            <ul className={`${Styles.faqCategory}`}>
              <li
                className={`${Styles.items} my-2 cursor-pointer transition`}
                onClick={() => setFilterCategory("")}
              >
                <span className={`${Styles.icon} transition`}>
                  <ArrowLeftShort size={25} />
                </span>
                همه دسته‌بندی ها
              </li>
              {categoryList.map((category, index) => {
                return (
                  <li
                    className={`${Styles.items} my-2 cursor-pointer pr-3 transition`}
                    key={index}
                    onClick={() => setFilterCategory(category)}
                  >
                    <span className={`${Styles.icon} transition`}>
                      <ArrowLeftShort size={25} />
                    </span>
                    {category}
                  </li>
                );
              })}
            </ul>
          </Col>
          <Col className="d-flex flex-wrap">
            <div className={`${Styles.searchFaq} mb-2`}>
              <CustomizeInput
                icon={() => <Search />}
                label="عنوان مورد نطر خود را وارد کنید"
                placeholder="جستجو"
                size="md"
                onChange={(e) => setSearch(e.target.value)}
              />
            </div>
            <ul className={`${Styles.faqList} f-100`}>
              {questions
                .slice(currentPageQuestions, currentPageQuestions + 5)
                .filter((item) => item.title.includes(filterCategory))
                .filter((item) => item.question.includes(search))
                .map((item, index) => {
                  return (
                    <li
                      className={`${Styles.items} my-3 cursor-pointer`}
                      key={index}
                      onClick={() => setActiveQuestionId(index)}
                    >
                      <div
                        className={`${Styles.header} px-4 py-3 d-flex justify-content-between align-items-center`}
                      >
                        <span>
                          <Check2Circle className="pl-1" size={20} />{" "}
                          {item.question}
                        </span>
                        <span>
                          <ChevronDown
                            size={20}
                            className={
                              activeQuestionId === index ? "rotate-180" : ""
                            }
                          />
                        </span>
                      </div>
                      {activeQuestionId === index ? (
                        <div className={`${Styles.body} px-4 py-3`}>
                          <span>{item.answer}</span>
                        </div>
                      ) : null}
                    </li>
                  );
                })}
            </ul>
            <div className="text-center w-100 mt-4">
              <Pagination
                totalRecords={50}
                pageLimit={5}
                pageNeighbours={2}
                handleChangePage={(data) => setCurrentPageQuestions(data)}
              />
            </div>
          </Col>
        </Row>
      </Container>
    </>
  );
};

export default Faq;

export async function getServerSideProps() {
  try {
    let res = await getQuestionsList();
    return {
      props: {
        questions: res.data.data,
      },
    };
  } catch (error) {
    return {
      props: {
        questions: [],
        error: error.message,
      },
    };
  }
}
