import { useState } from "react";
import Head from "next/head";
import { Container, Row, Col, Card } from "react-bootstrap";
import { getCoursersList } from "../../services";
import Styles from "./DeatailCourse.module.scss";
//components
import GridCourseCard from "../../components/course-card/grid-course/GridCourseCard";

const Courses = ({ courses, error }) => {
  console.log("index props", courses);
  return (
    <>
      <Head>
        <title> دوره ها</title>
        <meta name="viewport" content="initial-scale=1.0, width=device-width" />
      </Head>

      <div className={Styles.backgroundListCourses}>
        <Container>
          <div className={`${Styles.mainTitle} py-3`}>
            <h2 className={`${Styles.title} text-center`}>
              <span className="font-weight-bold">دوره های فعال</span> در
                  مجموعه
                </h2>
            <p className={`${Styles.text} text-center`}>
              تیم تحقیقات و بررسی ما به صورت روزانه با بررسی فرصت ها، سرفصل
              های جدید برای دوره های مناسب برای بهبود هرچه بیشتر زندگی مالی
              شما را تهیه و آماده میکند
                </p>
          </div>
        </Container>

      </div>
      <Row>

        <Container>
          <Row className="mt-5">
            {courses?.map((course, index) => {
              return (
                <Col  lg={4} md={6} sm={12} key={`coursor-${index}`} className="mb-4">
                  <GridCourseCard data={course} />
                </Col>
              );
            })}
          </Row>
        </Container>
      </Row>
    </>
  );
};

export default Courses;

export async function getServerSideProps() {
  try {
    const { data } = await getCoursersList();
    return {
      props: {
        courses: data.data,
      },
    };
  } catch (error) {
    return {
      props: {
        courses: [],
        error: error.message,
      },
    };
  }
}
