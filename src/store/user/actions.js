import { SET_TOKEN, DELETE_TOKEN, UPDATE_INFO } from "../actionTypes";

export const setTokenAction = (payload) => {
  return {
    type: SET_TOKEN,
    payload,
  };
};
export const delTokenAction = () => {
  return {
    type: DELETE_TOKEN,
  };
};
//
export const UpdateInfoAction = (payload) => {
  return {
    type: UPDATE_INFO,
    payload,
  };
};
