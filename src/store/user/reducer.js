import { SET_TOKEN, DELETE_TOKEN, UPDATE_INFO } from "../actionTypes";

const initialState = {};

const userReducer = (state = initialState, { type, payload }) => {
  switch (type) {
    case SET_TOKEN:
      return { ...payload };
    case DELETE_TOKEN:
      return {};
    case UPDATE_INFO:
      return { ...state, ...payload };
    default:
      return state;
  }
};
export default userReducer;
