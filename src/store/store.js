import { createStore, applyMiddleware } from 'redux';
import thunk from 'redux-thunk';
import logger from 'redux-logger';
import { composeWithDevTools } from 'redux-devtools-extension';
import RootReducer from './rootReducer';

import { persistReducer,persistStore } from 'redux-persist';
import storage from 'redux-persist/lib/storage';

const middlewares = [logger, thunk];

const persistConfig = {
    timeout: 0,
	key: 'root',
	storage,
};

const persistedReducer = persistReducer(persistConfig, RootReducer);

const store = createStore(persistedReducer, composeWithDevTools(applyMiddleware(...middlewares)));
 const persistor = persistStore(store);

export { store,persistor };
