import React from "react";

const IFrameComponent = ({ mapAddress }) => {
  return (
    <>
      {mapAddress ? (
        <iframe
          src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d202.41831732032938!2d51.345077318903435!3d35.733766161992904!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3f8dfd898dea7aed%3A0x257e2f49a2e078b8!2sAlvand%20Tower!5e0!3m2!1sen!2sfr!4v1609750872206!5m2!1sen!2sfr"
                frameborder="0"
          allowfullscreen=""
          aria-hidden="false"
          tabindex="0"
        ></iframe>
      ) : (
        ""
      )}
    </>
  );
};
export default IFrameComponent;
