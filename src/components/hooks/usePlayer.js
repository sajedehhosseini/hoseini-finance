import { useRef, useState, useEffect } from "react";
import videojs from "video.js";

const headers = {
  Authorization: "Bearer=" + "YOUR TOKEN",
};
export const usePlayer = ({ src, controls, autoplay }) => {
  const options = {
    fill: true,
    fluid: true,
    controls: true,
    preload: "auto",
    autoplay: true,
    headers,
    html5: {
      hls: {
        enableLowInitialPlaylist: true,
        smoothQualityChange: true,
        overrideNative: true,
      },
    },
  };
  const videoRef = useRef(null);
  const [player, setPlayer] = useState(null);

  useEffect(() => {
    const vjsPlayer = videojs(videoRef.current, {
      ...options,
      controls,
      autoplay,
      sources: [src],
    });
    setPlayer(vjsPlayer);

    return () => {
      if (player !== null) {
        vjsPlayer.dispose();
      }
    };
  }, []);
  useEffect(() => {
    if (player !== null) {
      vjsPlayer.src({ src, type: "application/x-mpegURL" });
    }
  }, [src]);

  return videoRef;
};
