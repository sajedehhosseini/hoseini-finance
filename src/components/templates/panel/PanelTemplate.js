import React from "react";
import Styles from "./PanelTemplate.module.scss";
import Link from "next/link";
import { useRouter } from "next/router";
import { Row, Col, Container } from "react-bootstrap";
import {
  PersonLinesFill,
  CollectionPlayFill,
  Headset,
  BoxArrowRight,
  Person,
  HouseDoor,
  BoxArrowLeft,
} from "react-bootstrap-icons";
import { Scrollbar } from "react-scrollbars-custom";
//store
import { useSelector, useDispatch } from "react-redux";
import { delTokenAction } from "../../../store/user/actions";
const PanelTemplate = ({ children }) => {
  const { pathname } = useRouter();
  const { phone, username } = useSelector((state) => state.user);
  const dispatch = useDispatch();
  return (
    <div className={`${Styles.panelContainer}`}>
      <Container className="h-100">
        <Row className={`${Styles.backgroundPanel}  h-100 `}>
          <Col className={`${Styles.side} py-5 px-3`}>
            <div
              className={`${Styles.userInfo} d-flex w-100 flex-wrap p-3 mb-5 align-items-center`}
            >
              <div className={Styles.icon}>
                <img src="/images/UserInfo.png" />
              </div>
              <div className={Styles.info}>
                <p className={`${Styles.name} mb-1`}>{username}</p>
                <p className={`${Styles.number} mb-0 FaNum`}>{phone}</p>
              </div>

              <ul
                className={`${Styles.button} d-flex justify-content-left align-items-center mb-0 px-3`}
              >
                <li className="mr-2 mb-0 cursor-pointer">
                  <Link href="/">
                    <a className="d-flex transition">
                      <HouseDoor />
                    </a>
                  </Link>
                </li>
                <li
                  className="mr-2 mb-0 cursor-pointer"
                  onClick={() => dispatch(delTokenAction())}
                >
                  <Link href="/user">
                    <a className="d-flex transition">
                      <BoxArrowLeft />
                    </a>
                  </Link>
                </li>
                <li className="mr-2 mb-0 cursor-pointer">
                  <Link href="/user/profile">
                    <a className="d-flex transition">
                      <Person />
                    </a>
                  </Link>
                </li>
              </ul>
            </div>
            <ul className={`${Styles.sideItems}  transition`}>
              <li
                className={`transition ${
                  pathname === "/user/profile" ? Styles.active : ""
                }`}
              >
                <PersonLinesFill className="ml-3 transition" />
                <Link href="/user/profile">
                  <a className="transition text-right">پروفایل کاربری</a>
                </Link>
              </li>
              <li
                className={`transition ${
                  pathname === "/user/coursers" ? Styles.active : ""
                }`}
              >
                <CollectionPlayFill className="ml-3 transition" />
                <Link href="/user/courses">
                  <a className="transition text-right">دوره‌های خریداری شده</a>
                </Link>
              </li>
              <li
                className={`transition ${
                  pathname === "/user/ticket" ? Styles.active : ""
                }`}
              >
                <Headset className="ml-3 transition" />
                <Link href="/user/ticket">
                  <a className="transition text-right">پشتیبانی</a>
                </Link>
              </li>
              <li
                className="transition"
                onClick={() => dispatch(delTokenAction())}
              >
                <BoxArrowRight className="ml-3 transition" />
                <Link href="/user">
                  <a className="transition text-right">خروج</a>
                </Link>
              </li>
            </ul>
          </Col>

          <Col lg={12} className={`${Styles.panel} pr-5`}>
            <div className={`${Styles.bodyPanel} p-3`}>
              <Scrollbar className="hiddenScroll" style={{ height: 430 }}>
                {children}
              </Scrollbar>
            </div>
          </Col>
        </Row>
      </Container>
      <div className={`${Styles.responsiveSide} `}>
        <div
          className={` ${pathname === "/user/profile" ? Styles.active : ""} ${
            Styles.items
          }`}
        >
          <Link href="/user/profile">
            <PersonLinesFill className=" transition" />
          </Link>
        </div>
        <div
          className={` ${pathname === "/user/coursers" ? Styles.active : ""} ${
            Styles.items
          }`}
        >
          <Link href="/user/courses">
            <CollectionPlayFill className=" transition" />
          </Link>
        </div>
        <div
          className={` ${pathname === "/user/ticket" ? Styles.active : ""} ${
            Styles.items
          }`}
        >
          <Link href="/user/ticket">
            <Headset className=" transition" />
          </Link>
        </div>
        <div
          className={Styles.items}
          onClick={() => dispatch(delTokenAction())}
        >
          <Link href="/user">
            <BoxArrowRight className=" transition" />
          </Link>
        </div>
      </div>
    </div>
  );
};

export default PanelTemplate;
