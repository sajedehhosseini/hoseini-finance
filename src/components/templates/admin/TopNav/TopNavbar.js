import React, { Component } from 'react';
import Styles from './TopNavbar.module.scss';
import {useDispatch} from 'react-redux';
import {Button} from 'react-bootstrap';
import {delTokenAction} from '../../../../store/user/actions';
const TopNavbar = () => {
	const dispatch = useDispatch()
	return (
		<nav className={`navbar fixed-top ${Styles.navbar}`}>
			<div>
				logo
			</div>
			<div className="user d-inline-block">
				<span className="name ml-1">فاطمه کاظمی زاده</span>
				<span>
					<img alt="Profile" src="/assets/img/profile-pic-l.jpg" />
				</span>
				<Button onClick={()=>dispatch(delTokenAction())}>خروج</Button>
			</div>
		</nav>
	);
};

export default TopNavbar;
