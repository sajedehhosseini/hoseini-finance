import React, { Component, Fragment } from 'react';
import { Row, Col } from 'react-bootstrap';
import TopNavbar from './TopNav/TopNavbar';
import SideBar from './Sidebar/SideBar';
const AdminTemplate = ({ children }) => {
	return (
		<>
			<Row>
				<TopNavbar />
			</Row>
			<Row className="h-100 pt-8">
				<Col xs={2} className="pt-3">
					<SideBar />
				</Col>
				<Col xs={9} className="pt-5">
					<div className="container-fluid">{children}</div>
				</Col>
			</Row>
		</>
	);
};
export default AdminTemplate;
