export const cousers = [
    {title:'دوره ها',url:'courses'},
    {title:'دسته بندی',url:'categories'},
    {title:'سفارش ها',url:'orders'},
    {title:'کد تخفیف',url:'discount'},
]
export const users =[
    {title:'کاربران',url:'users'},
    {title:'ادمین ها',url:'admins'},
]
export const support = [
    {title:'تیکت ها',url:'tickets'},
]
export const setting = [
    {title:'تنظیمات',url:'setting'},
]
