import Styles from './TitlePanel.module.scss';

const TitlePanel = ({ title, titleClassName }) => {
    return (
        <div className={`${Styles.titleBox} mb-4`}>
            <span className={`${Styles.title} ${Styles[`${titleClassName}`]}`}>{title}</span>
        </div>
    );
};

export default TitlePanel;
