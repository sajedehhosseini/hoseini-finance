import React from "react";
import { Row, Col } from 'react-bootstrap';
import Styles from "./HeadingCard.module.scss";
import PulseAnimationButton from "../pulse-animation-button/PulseAnimationButton";
const HeadingCart = ({
  background='hoseini.png',
  children,
  title,
  statics: Statics,
  iconButton: IconButton,
}) => {
  return (
    
    <Row className="h-100 p-wide">
    
      <Col lg={6} md={6} sm={12} style={{ backgroundImage: `url(./images/${background})` }}
        className={`${Styles.headingImg} d-flex align-content-between align-items-between justify-content-center flex-wrap py-8`}
      >
         
          {title ? (
             
              <h1 className={`${Styles.titleHeader} p-relative mb-3`}>
              <span className={Styles.greenHeader}>{title.split(" ")[0]}</span>{" "}
              <span className={Styles.whiteHeader}>{title.split(" ").slice(1).join(' ')}</span>
            </h1>
             
          ) : null}
          {
              IconButton? <PulseAnimationButton icon={() => <IconButton />} /> : null
          }
          {Statics ? <Statics icon={() => <PersonIcon />} /> : " "}
          
      </Col>

      <Col lg={6} md={6} sm={12} className="py-lg-0 py-md-0 py-3">
        {children}
      </Col>

    </Row>
   
  
  );
};
export default HeadingCart;

// PulseAnimationButton
