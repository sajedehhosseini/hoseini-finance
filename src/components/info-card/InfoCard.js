import { Card, Row, Col } from 'react-bootstrap';

const InfoCard = ({ imageUrl, children }) => {
	return (
		<Card className="bg-green py-0 border-green">
			<Card.Body>
				<Row className="align-items-center">
					<Col xs={3} className=" py-0">
						<img src={imageUrl} alt="" />
					</Col>
					<Col xs={9} className="text-justify text-white  py-0">
						<p>{children}</p>
					</Col>
				</Row>
			</Card.Body>
		</Card>
	);
};

export default InfoCard;
