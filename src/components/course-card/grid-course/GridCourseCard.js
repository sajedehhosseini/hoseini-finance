import { useEffect, useState } from "react";
import { Card, Button } from "react-bootstrap";
import Styles from "./GridCourseCard.module.scss";
import Link from 'next/link';
import { Cart4, ClockFill, LayersHalf } from "react-bootstrap-icons";
import { formatNumber, decodeTimeForTimer } from "../../../configs/utils";
const GridCourseCard = ({ data }) => {
  console.log(data); //off_time
  const [dayTimer, setDayTimer] = useState();
  const [hrsTimer, setHrsTimer] = useState();
  const [minsTimer, setMinsTimer] = useState();
  useEffect(() => {
    if (data && data.off_time) {
      const { day, mins, hrs } = decodeTimeForTimer(data.off_time);
      setDayTimer(day);
      setHrsTimer(hrs);
      setMinsTimer(mins);
      setInterval(() => {
        setMinsTimer((prev) => prev - 1);
        if (minsTimer === 0) {
          setHrsTimer((prev) => prev - 1);
          setMinsTimer(59);
        }
        if (hrsTimer === 0) {
          setDayTimer((prev) => prev - 1);
        }
      }, 60000);
    }
    return () => clearInterval();
  }, [data]);
  return (
    <Card className={`${Styles.CourserCard} text-right`}>
      <div className={Styles.imgCard}>
        <Card.Img variant="top" src="./images/courserPic.png" />
        {data.off_time ? (
          <div className={Styles.counterBox}>
            <span className={Styles.counter}>
              <span className="FaNum">{dayTimer}</span>
              <p className={`${Styles.subNum} mb-0`}>روز</p>
            </span>
            <span className={Styles.counter}>
              <span className="FaNum">{hrsTimer}</span>
              <p className={`${Styles.subNum} mb-0`}>ساعت</p>
            </span>
            <span className={Styles.counter}>
              <span className="FaNum">{minsTimer}</span>
              <p className={`${Styles.subNum} mb-0`}>دقیقه</p>
            </span>
          </div>
        ) : null}
      </div>

      <Card.Body className="d-flex flex-wrap align-content-between p-3">
        <Card.Title className={`${Styles.title} f-100`}>
          {data.title}
        </Card.Title>

        <Card.Text className={`${Styles.text} f-100`}>
          {data.desc_sm}{" "}
        </Card.Text>

        <div
          className={`${Styles.footer} pt-3 d-flex flex-wrap align-items-stretch f-100`}
        >
          <div className={Styles.info}>
            <p className={Styles.text}>
              <LayersHalf className={Styles.icon} /> سطح دوره: {data.level}
            </p>
            {/* <p>مدت آموزش: {data.total_time}</p> */}
            <Button variant="success" className={`${Styles.button} bg-green`}>
              مشاهده و خرید
            </Button>
          </div>
          <div className={Styles.price}>
            {data.off_time ? (
              <>
                <p className="f-100 text-left">
                  <span className={`${Styles.oldPrice} FaNum`}>
                    {" "}
                    {formatNumber(data.old_price)}{" "}
                  </span>
                  <span className={Styles.saleTitle}>تخفیف ویژه</span>
                </p>
                <p className="f-100 text-left mb-0"><span className={`${Styles.mainPrice} FaNum`}>{data.price} <span className={Styles.currency}>تومان</span></span></p>
              </>
            ) : (
                <>
                  <p className="f-100 text-left">
                    <span className={`${Styles.mainPrice} FaNum`}>{data.price} <span className={Styles.currency}>تومان</span></span>
                  </p>
                </>
              )}
          </div>


          <div className=" f-100">

          </div>
        </div>
      </Card.Body>
    </Card>
  );
};

export default GridCourseCard;

// bought: false
// category: "دسته بندی 2"
// desc_sm: "hassanasdsdsad"
// id: 1
// image: "/media/productimage/are1_B418KJq.jpg"
// level: "ziad"
// off_time: ""
// old_price: 1500
// title: "asdadsaasd"
// total_time: "1"
