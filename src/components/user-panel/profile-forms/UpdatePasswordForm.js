import { useState, useEffect } from "react";
import { Person, Lock, Envelope } from "react-bootstrap-icons";
import { useForm } from "react-hook-form";
import { yupResolver } from "@hookform/resolvers/yup";
import { updatePassSchema } from "../../../configs/formValidateSchema";
import { updateUserInfo } from "../../../services";
//store
import { useDispatch } from "react-redux";
import { delTokenAction } from "../../../store/user/actions";
//components
import CustomizeInput from "../../form/input/Input";
import CustomizeButton from "../../form/button/Button";

const UpdatePasswordForm = ({ successHandle }) => {
  const dispatch = useDispatch();
  const [serverError, setServerError] = useState();
  const { handleSubmit, register, errors, reset } = useForm({
    resolver: yupResolver(updatePassSchema),
  });
  const formData = new FormData();
  
  const handleChangeData = async ({ password, password2 }) => {
   
    formData.append("password", password);
    formData.append("password2", password2);
    try {
      const { status, data } = await updateUserInfo(formData);

      {
        if (status === 200) {
          successHandle(data.message);
          dispatch(delTokenAction());
          localStorage.clear("token");
        }
      }
    } catch (error) {
      console.log(error);
    } finally {
    }
  };
  return (
    <>
      <CustomizeInput
        type="password"
        size="md"
        icon={() => <Lock />}
        label="رمز عبور قدیمی"
        ref={register()}
        name="password"
        errorMessage={errors.password && errors.password.message}
      />
      <CustomizeInput
        type="password"
        size="md"
        icon={() => <Lock />}
        label="رمز عبور جدید"
        ref={register()}
        name="password2"
        errorMessage={errors.password2 && errors.password2.message}
      />
      <div className="text-left mt-4">
        <CustomizeButton
          size="sm"
          onClick={handleSubmit(handleChangeData)}
          className="logRegSubmit"
        >
          تغییر رمز عبور
        </CustomizeButton>
      </div>
    </>
  );
};

export default UpdatePasswordForm;
