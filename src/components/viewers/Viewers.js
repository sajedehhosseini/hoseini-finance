import React from 'react';
import Styles from "./Viewers.module.scss";
import { Col } from 'react-bootstrap';
import SwiperComponent from "../swiper/Swiper";
import { PlayFill } from "react-bootstrap-icons";
import PulseAnimationButton from "../pulse-animation-button/PulseAnimationButton";

const swiperParams = {

    spaceBetween: 10,
    freeMode: true,
    navigation: {
        nextEl: ".swiper-button-next",
        prevEl: ".swiper-button-prev",
    },
    breakpoints: {
        320: {
            slidesPerView: 1,
        },
        768: {
            slidesPerView: 2,
        },
        991: {
            slidesPerView: 3,
        },
    }
};

const viewersData = [

    { img: "/images/viewers.png", title: "محمد رمضانی", subtitle: "دانشجو دوره جامع ارزهای دیجیتال", desc: "بیان واضح و عالی دوره ها به شدت جذاب و دوره کاربردی بود ، با تشکر", date: "1399/08/05" },
    { img: "/images/viewers.png", title: "محمد رمضانی", subtitle: "دانشجو دوره جامع ارزهای دیجیتال", desc: "بیان واضح و عالی دوره ها به شدت جذاب و دوره کاربردی بود ، با تشکر", date: "1399/08/05" },
    { img: "/images/viewers.png", title: "محمد رمضانی", subtitle: "دانشجو دوره جامع ارزهای دیجیتال", desc: "بیان واضح و عالی دوره ها به شدت جذاب و دوره کاربردی بود ، با تشکر", date: "1399/08/05" },
    { img: "/images/viewers.png", title: "محمد رمضانی", subtitle: "دانشجو دوره جامع ارزهای دیجیتال", desc: "بیان واضح و عالی دوره ها به شدت جذاب و دوره کاربردی بود ، با تشکر", date: "1399/08/05" }

]
const ViewersCard = () => {
    return (
        <SwiperComponent params={swiperParams} >
            {
                viewersData.map((item, index) => {
                    return (
                        <div className="my-3 px-2" key={index}>
                            <div className={`${Styles.ViewersCard} w-100 d-flex align-content-between p-2 flex-wrap h-100`}>

                                <div className={`${Styles.top} w-100 d-flex f-100 align-items-center`}>
                                    <img src={item.img} />
                                    <span className="mr-2 d-flex flex-wrap">
                                        <span className="mb-2">{item.title}</span>
                                        <span className={Styles.subtitle}>{item.subtitle}</span>
                                    </span>
                                </div>
                                <div className="w-100 d-flex  f-100 my-3 px-3">
                                    <p className="text mb-0">{item.desc}</p>
                                </div>
                                <div className="w-100 f-100 text-left">
                                    <span className={Styles.subtitle}>{item.date}</span>
                                </div>
                            </div>
                        </div>
                    )
                })
            }
        </SwiperComponent>

    );
};

export default ViewersCard;
