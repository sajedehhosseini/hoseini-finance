import React from 'react';
import Styles from "./OurTeam.module.scss";

import { Container, Row, Col, Card } from 'react-bootstrap';

const OurTeam = ({ pic, name, position }) => {

    return (
        <>
            <Col lg={3} md={4} sm={6} className="mb-4">
                <Card className={`${Styles.teamCard} h-100`}>
                    <Card.Img variant="top" className={Styles.teamPic} src="./images/ourteam.jpg" />
                    <Card.Body className={`${Styles.cardBody} text-center`}>
                        <Card.Title className={Styles.title}>سامان یوسفی</Card.Title>
                        <Card.Text className={`${Styles.subTitle} text-green`}>مدیر اجرایی</Card.Text>
                    </Card.Body>
                </Card>
            </Col>
        </>
    )
}
export default OurTeam;