import React from "react";
import { Alert } from "react-bootstrap";
import Styles from "./Alert.module.scss";

const CustomizeAlert = ({ children, variant, onClose , dismissible}) => {
    return (
        <>
            <Alert className={Styles.alert} variant={variant} onClose={onClose} dismissible={dismissible}>
                {children}
            </Alert>
        </>
    );
};
export default CustomizeAlert;
