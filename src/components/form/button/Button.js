import React from "react";
import Button from "react-bootstrap/Button";

const CustomizeButton = ({
  type = "button", //button,submit,reset
  variant = "success",
  outlined = false,
  isFullwidth = false,
  disable = false,
  isLoading = false,
  size = "md", //lg,sm,md
  as = "",
  className,
  icon: Icon,
  children,
  onClick,
  ...rest
}) => {
  return (
    <Button
      type={type}
      as={as}
      variant={`${outlined ? "outline-" : ""}${variant}`}
      className={className}
      size={size}
      onClick={!isLoading ? onClick : null}
      block={isFullwidth ? true : false}
      disabled={isLoading || disable}
      {...rest}
    >
      {Icon ? <Icon /> : ""}
      {isLoading ? "Loading…" : children || 'button'}
    </Button>
  );
};
export default CustomizeButton;
