import React, { forwardRef } from "react";
import { Form } from "react-bootstrap";

const list = ["a", "b", "c", "d"];

const CustomizeSelect = forwardRef(({name, label, options=[],errorMessage }, ref) => {
  return (
    <>
      <Form.Group controlId="exampleForm.ControlSelect1">
        {label ? <Form.Label>{label}</Form.Label> : null}
        <Form.Control name={name} as="select" ref={ref}>
          {options.map((item,index) => {
            return <option key={index}>{item}</option>;
          })}
        </Form.Control>
      </Form.Group>
    </>
  );
});
export default CustomizeSelect;
