import React, { forwardRef, useState } from "react";
import { Form, InputGroup } from "react-bootstrap";

import { EyeFill, EyeSlashFill } from "react-bootstrap-icons";

const CustomizeInput = forwardRef(
  (
    {
      name = "",
      label = "",
      icon: Icon,
      className = "",
      type = "text",
      placeholder = "",
      size = "md",
      multiline = false,
      readOnly = false,
      disabled = false,
      rows,
      rowsMax,
      errorMessage,
      onChange,
    },
    ref
  ) => {
    const [showPass, setShowPass] = useState(false);

    return (
      <>
        <Form.Group controlId="formBasicEmail">
          {label ? (
            <Form.Label>
              {Icon ? <Icon /> : null}
              {label}
            </Form.Label>
          ) : (
            ""
          )}
          <InputGroup>
            {type === "password" ? (
              <InputGroup.Prepend>
                <InputGroup.Text>
                  {showPass ? (
                    <EyeSlashFill onClick={() => setShowPass(false)}/>
                  ) : (
                    <EyeFill onClick={() => setShowPass(true)} />
                  )}
                </InputGroup.Text>
              </InputGroup.Prepend>
            ) : null}

            <Form.Control
              type={
                type === "password" ? (showPass ? "text" : "password") : type
              }
              name={name}
              rows={rows}
              size={size}
              placeholder={placeholder}
              label={label}
              readOnly={readOnly}
              disabled={disabled}
              ref={ref}
              as={rows && "textarea"}
              isInvalid={errorMessage ? true : false}
              onChange={onChange}
              // isValid={true}
            />
          </InputGroup>
          {errorMessage ? (
            <Form.Text className="text-danger">{errorMessage}</Form.Text>
          ) : null}
        </Form.Group>
      </>
    );
  }
);
export default CustomizeInput;

// <Form.Text className="text-muted">{errorMessage}</Form.Text>
