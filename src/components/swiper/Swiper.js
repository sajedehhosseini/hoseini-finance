import Swiper from "react-id-swiper";
// import "swiper/swiper.scss";
// import "swiper/components/navigation/navigation.scss";
// import "swiper/components/pagination/pagination.scss";
const SwiperComponent = ({ children, params }) => {
  // const params = {
  //   navigation: {
  //     nextEl: ".swiper-button-next",
  //     prevEl: ".swiper-button-prev",
  //   },
  // };
  return <Swiper containerClass="align-items-stretch" {...params}>{children}</Swiper>;
};

export default SwiperComponent;
