import React from 'react';
import Styles from "./CertificateSteps.module.scss";
import { Col } from 'react-bootstrap';


const certificateData = [

    { img: "/images/document.png", title: "ثبت نام در دوره جامع ارزهای دیجیتال " },
    { img: "/images/video-editing.png", title: "تکمیل مشخصات پنل کاربری " },
    { img: "/images/password.png", title: "پیگیری مدرک  " }
]
const CertificateSteps = () => {
    return (
        <>
            {
                certificateData.map((item,index) => {
                    return (
                        <Col key={index} lg={4} md={4} sm={12} className={`${Styles.certificateBox} px-lg-5 px-1 mb-3`}>
                            <div className={`${Styles.certificateSteps} h-100 d-flex justify-content-center flex-wrap p-5`}>

                                <div className={`${Styles.imageBox} mb-4`}>
                                    <span className={`${Styles.image} transition bg-green`}>
                                        <img src={item.img} />
                                    </span>
                                </div>
                                <div className={Styles.title}><span>{item.title}</span></div>
                            </div>
                        </Col>
                    )
                })
            }
        </>

    );
};

export default CertificateSteps;
