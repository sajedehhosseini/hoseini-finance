import React, { useEffect, useState } from "react";
import Styles from "./Menu.module.scss";
import { useRouter } from "next/router";
import Link from "next/link";
import { useSelector } from "react-redux";
const Menu = ({ activeTheme }) => {
  const { pathname } = useRouter();
  const state = useSelector((state) => state.user);
  return (
    <div className={`${Styles.rspeFixedMenu} flex-wrap align-items-center`}>
      <div
        className={`${Styles.rspeFixedMenuItems} ${
          pathname === "/" ? Styles.active : " "
        }`}
      >
        {" "}
        <Link href="/">
          <a className="text-center">
            <img src="/icons/home_IC.png" alt="Home" />
            <p className="mb-0 text-center">خانه</p>
          </a>
        </Link>
      </div>
      <div
        className={`${Styles.rspeFixedMenuItems} ${
          pathname === "/courses" ? Styles.active : " "
        }`}
      >
        {" "}
        <Link href="/courses">
          <a className="text-center">
            <img src="/icons/category_IC.png" alt="Coursers" />
            <p className="mb-0 text-center">دوره ها</p>
          </a>
        </Link>
      </div>
      <div
        className={`${Styles.rspeFixedMenuItems} ${
          pathname.split("/").slice(1)[0] === "user" ? Styles.active : " "
        }`}
      >
        {" "}
        <Link href="/user">
          <a className="text-center">
            <img src="/icons/user_IC.png" alt="Profil" />
            {state && state.access ? (
              <p className="mb-0 text-center">پنل کاربری</p>
            ) : (
              <p className="mb-0 text-center">ورود / ثبت نام</p>
            )}
          </a>
        </Link>
      </div>
    </div>
  );
};
export default Menu;
