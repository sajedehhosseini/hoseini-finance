import React from "react";
import Styles from "./Footer.module.scss";

import { Container, Row, Col } from 'react-bootstrap';

import { PersonFill, KeyFill, TelephoneFill, PatchQuestionFill, BookHalf, CreditCardFill, Twitter, Instagram, Linkedin, Facebook } from 'react-bootstrap-icons';



// import IFrameComponent from "../../components/iframe/IFrame";


const Footer = () => {
  return (

    <Row className={`${Styles.footer} align-items-stretch`}>
      <Col className={`${Styles.md} p-0`}>
        <div className="d-flex flex-wrap align-items-center pl-3 pr-5 pt-10">
          <Col xs={12} className={Styles.footerLogo}>
            <img src="/images/footer-logo.png" alt="Hoseini Finance" />
          </Col>
          <Col xs={12} className={`${Styles.footerAbout} p-0`}>
            <p>
              تهران خیابان مرزداران خیابان ابراهیمی برج الوند طبقه دوازدهم واحد ۱۲۰۱
            </p>
            <p className={Styles.contact}>
              <a className="mb-3 FaNum" href="tel:02144258068">021-44258068</a>
              <span className="mb-3">شنبه تا چهارشنبه: 9 صبح - 5 بعد از ظهر</span>
              <span>پنجشنبه: 9 صبح - 2 بعد از ظهر</span>
            </p>
          </Col>
          <Col xs={12} className={`${Styles.footerSocial} p-0 mt-4`}>
            <ul className="d-flex justify-content-between align-items-center">
              <li><Instagram /></li>
              <li><Facebook /></li>
              <li><Linkedin /></li>
              <li><Twitter /></li>
              <li><img src="./icons/telegram.png" /></li>
              <li><img src="./icons/aparat.png" /></li>
            </ul>
          </Col>
        </div>
      </Col>
      <Col className={`${Styles.lg} p-0 pr-4 d-flex justify-content-center align-items-center`}>
        <div className="d-flex w-100 flex-wrap">
          <Col lg={4} md={4} sm={12} className="p-0 px-lg-0 px-3">
            <ul>
              <li>لینک های مهم</li>
              <li><PersonFill className={Styles.icon} /> پنل کاربری</li>
              <li><KeyFill className={Styles.icon} /> فراموشی رمز عبور</li>
              <li><TelephoneFill className={Styles.icon} /> پشتیبانی</li>
              <li><PatchQuestionFill className={Styles.icon} /> سوالات متداول</li>
            </ul>
          </Col>
          <Col lg={4} md={4} sm={12} className="p-0 px-3">
            <ul>
              <li>دوره ها</li>
              <li> دوره جامع ارزهای دیجیتال</li>
              <li>دوره استخراج ارزهای دیجیتال</li>
              <li>دوره جامع بازارهای جهانی</li>
            </ul>
          </Col>
          <Col lg={4} md={4} sm={12} className={`p-0 px-lg-0 px-3`}>
            <ul>
              <li>موارد قانونی</li>
              <li><BookHalf className={Styles.icon} /> قوانین و شرایط استفاده</li>
              <li><CreditCardFill className={Styles.icon} /> شرایط پرداخت</li>
            </ul>

          </Col>
        </div>
      </Col>
      <Col className={`${Styles.sm} p-0`}>
        <img src="/images/map-footer.png" />
      </Col>
    </Row>

  );
};
export default Footer;
