import React from "react";
import Styles from "./MenuItem.module.scss";
import Link from "next/link";
import { useRouter } from "next/router";

const MenuItem = ({ title, url }) => {
  const router = useRouter();
  return (
    <li
      className={`ml-4 flex-row align-items-center
      ${url === router.pathname ? Styles.headerAfter : " "}
      `}
    >
      <Link href={url}>
          <a className="ml-1">{title}</a>
      </Link>
    </li>
  );
};
export default MenuItem;
